import React, { Component } from "react";
import { connect } from "react-redux";
import { reduxForm, Field } from "redux-form";
import propTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment'
import PhoneInput from "react-phone-number-input";
import { isValidPhoneNumber } from "react-phone-number-input";
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import "react-phone-number-input/style.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  map as _map,
  pick as _pick,
  partialRight as _partialRight,
  filter as _filter
} from "lodash";

import CompleteBookingLogin from "../container/login/completeBookingLoginModel"
//import CompleteBookingLogin from "../container/login/completeBookingLoginModel"

import { getCard } from '../../service/card/action'
import URL from '../../asset/configUrl';
import { payment } from "../../service/payment/action";

import { carBooking, deletePaymentProps ,carGetPayload} from '../../service/car/action'
import { bookingSignUpInfo ,signupReset} from "../../service/login/action";

import InputField from "../Fields/TextField";
import SelectField from "../Fields/SelectField";
import countryList from 'react-select-country-list'
import Select from 'react-select'
import SignInModal from "../container/login/SignInModal"

import img_paymentCard from "../../asset/images/paymentCard1.png";

import queryString from "query-string";

import DateRangePicker from "react-daterange-picker";


class CarPayment extends Component {

  state = {
    isModal: false,
    card: [],
    selected: [],
    phone: "",
    concatError: "",
    contactValid: true,
    country: "",
    region: "",
    countryFilter: "",
    isdivHide: true,
    isModalOpen: false,
    onCardSelected: false,
    isDob: false,
    sameAsCreditCard: false,
    firstNameDisabled: false,
    dob: moment().subtract('years', 18),
    options: countryList().getData(),
    nationality: '',
    cardName: "",
    startDate:moment().subtract(17,'years').toDate(),
  };
  
  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleFormInitialValues = () => {

    this.setState({
      cardName: "" || (this.state.selected && this.state.selected.name)
    })

    if (this.state.sameAsCreditCard) {
      this.props.change("firstName", "" || (this.state.selected && this.state.selected.name))
    }

    this.props.change("cardName", "" || (this.state.selected && this.state.selected.name))
    this.props.change("cardNumber", "" || (this.state.selected && this.state.selected.last4))
    this.props.change("cvv", "")
    this.props.change("month", "" || (this.state.selected && this.state.selected.exp_month))
    this.props.change("year", "" || (this.state.selected && this.state.selected.exp_year))


    // this.props.initialize({
    //   firstName: "" || (this.state.selected && this.state.selected.name),
    //   cardNumber: "" || (this.state.selected && this.state.selected.last4),
    //   cardName: "" || (this.state.selected && this.state.selected.name),
    //   cvv: "",
    //   month: "" || (this.state.selected && this.state.selected.exp_month),
    //   year: "" || (this.state.selected && this.state.selected.exp_year)
    // });
  };

  getCard = (nextProps) => {
    let config = {
      headers: {
        "secret-code": "xeni-app-development"
      }
    };
    if (nextProps) {
      const { email } = nextProps;
      axios.get(
        URL.card.CARD_GET + email,
        config
      )
        .then(response => {
          this.setState({ card: response.data.data.data });
        });
    }
  };
 
  componentDidMount() {
    axios.get("https://restcountries.eu/rest/v2/all").then(response => {
      this.setState({
        countryFilter: _map(
          response.data,
          _partialRight(_pick, ["name", "alpha2Code"])
        )
      });
    });
    this.props.signupReset();
  }

  handleCardSelect = e => {
    if (e.target.value) {
      this.setState(
        {
          selected: this.state.card[e.target.value],
          onCardSelected: true
        },
        () => this.handleFormInitialValues()
      );
    } else {
      this.setState({
        onCardSelected: false,
        selected: { id: "" }
      })
      this.props.change("cardName", "")
      this.props.change("cardNumber", "")
      this.props.change("cvv", "")
      this.props.change("month", "")
      this.props.change("year", "")

    }
  };

  componentWillReceiveProps(nextProps) {
        

    try {
      if (nextProps.loginDetails) {
        if (this.state.card && this.state.card.length == 0) {
          this.getCard(nextProps.loginDetails);
        }
      } else {
        this.setState({ card: [] })
      }
    } catch (e) {
      
    }

    if (nextProps.hasOwnProperty('paymentDetails')) {
      if (nextProps.paymentDetails != this.props.paymentDetails) {
        if (nextProps.paymentDetails.hasOwnProperty("data") && nextProps.paymentDetails.data == 'success') {
          const addedQueryString = queryString.parse(this.props.location.search)
          this.props.history.push("/carbookingconfirmation?" + queryString.stringify({ ...addedQueryString, bookingId: nextProps.paymentDetails.completedBookingIds[0] }));
        } else if (nextProps.paymentDetails.hasOwnProperty("error")) {
          this.setState({ isModal: true });
          //this.setState({ isModalOpen: false })
        }
      }
    }

    const { gustLoginCar } = nextProps;

    if(gustLoginCar === 'failure' || gustLoginCar){
      this.setState({ isModalOpen: true })
     
    }

    if(gustLoginCar === 'success'){
      this.setState({ isModalOpen: false })
      //this.setState({ isModal: true });
    }
  }

  selectCountry(val) {
    this.setState({ country: val });
  }

  selectRegion(val) {
    this.setState({ region: val });
  }

  handleModal = () => {
    this.setState({ isModal: !this.state.isModal });
  };

  handleSelectCard = e => {
    this.setState({ card: e.target.value });
  };

  close = () => {
    this.setState({ isModalOpen: false })
  }

  generatePassword = () =>  {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

  changeHandlerNationality = nationality => {
    this.setState({ nationality })
  }

  handleBooking = value => {
   
    if (this.props.loginStatus === false || this.props.loginStatus === undefined) {

      const guestFlag = "CarBookingFlag";
      const  email = value.email;

      let paymentCardId = this.state.selected.id

      const payloadInfo = [{
        sessionId: this.props.carPricedetails.sessionId,
        email,
        rentalId: this.props.carPricedetails.carRental.id,
        "paymentBreakup": [
          {
            // "paymentMethodRefId": "ea99994f-fdda-465c-9081-e65b7092b99f",
            "amount": +this.props.carPricedetails.quotedTotalFare,
            "currency": this.props.selectedCurrency,
            "type": "Cash"
          }
        ],
        "actualPaymentBreakup": [
          {
            "amount": +this.props.carPricedetails.pricedTotalFare,
            "currency": this.props.selectedCurrency,
            "type": "Cash"
          }
        ],
        "paymentMethod": {
          "card_id": paymentCardId,
          "cards": [
            {
              "refId": "ea99994f-fdda-465c-9081-e65b7092b99f",
              "num": value.cardNumber,
              "nameOnCard": value.cardName,
              "cvv": value.cvv,
              "issuedBy": "VI",
              "expiry": {
                month: +value.month,
                year: +value.year
              },
              "contactInfo": {
                "phones": [
                  {
                    "type": "Mobile",
                    "num": +this.state.phone,
                    "countryCode": "1",
                    "ext": "123"
                  }
                ],
                "billingAddress": {
                  line1: value.address,
                  city: {
                    code: "SFO",
                    name: value.city
                  },
                  state: {
                    code: this.state.region,
                    name: this.state.region
                  },
                  "countryCode": this.state.country,
                  "postalCode": value.zipcode
                },
                email: value.email,
              }
            }
          ]
        },
        "customer": {
          "name": {
            first: value.firstName,
            last: value.lastName
          },
          "contactInfo": {
            "address": {
              line1: value.address,
              city: {
                code: "SFO",
                name: value.city
              },
              state: {
                code: this.state.region,
                name: this.state.region
              },
              "countryCode": this.state.country,
              "postalCode": value.zipcode
            },
            email: value.email,
            "phones": [
              {
                "type": "Mobile",
                "num": +this.state.phone,
                "countryCode": "1",
                "ext": "123"
              }
            ]
          },
          "dob": moment(this.state.dob).format('YYYY-MM-DD'),
          "nationality": this.state.nationality.value,
          "customerId": "43435"
        },
        "driverInfo": {
          "name": {
            first: value.firstName,
            last: value.lastName
          },
          "contactInfo": {
            "address": {
              line1: value.address,
              city: {
                code: "SFO",
                name: value.city
              },
              state: {
                code: this.state.region,
                name: this.state.region
              },
              "countryCode": this.state.country,
              "postalCode": value.zipcode
            },
            email: value.email,
            "phones": [
              {
                "type": "Mobile",
                "num": +this.state.phone,
                "countryCode": "1",
                "ext": "123"
              }
            ]
          },
          "dob": moment(this.state.dob).format('YYYY-MM-DD'),
          "nationality":this.state.nationality.value
        },
        carDetails: this.props.carPricedetails,
        carBookingParameters: queryString.parse(this.props.location.search)
      }]

      const payload = {
        name: value.firstName,
        email: value.email ,
        password: this.generatePassword()
      };

      sessionStorage.setItem("loginInfoBooking", JSON.stringify(payload));
      this.props.bookingSignUpInfo( guestFlag ,payload ,payloadInfo)
      this.props.carGetPayload(payloadInfo)
   
    }
    else {
   
      const { email } = this.props.loginDetails && this.props.loginDetails;

      let paymentCardId = this.state.selected.id
      const payload = [{
        sessionId: this.props.carPricedetails.sessionId,
        email,
        rentalId: this.props.carPricedetails.carRental.id,
        "paymentBreakup": [
          {
            // "paymentMethodRefId": "ea99994f-fdda-465c-9081-e65b7092b99f",
            "amount": +this.props.carPricedetails.quotedTotalFare,
            "currency": this.props.selectedCurrency,
            "type": "Cash"
          }
        ],
        "actualPaymentBreakup": [
          {
            "amount": +this.props.carPricedetails.pricedTotalFare,
            "currency": this.props.selectedCurrency,
            "type": "Cash"
          }
        ],
        "paymentMethod": {
          "card_id": paymentCardId,
          "cards": [
            {
              "refId": "ea99994f-fdda-465c-9081-e65b7092b99f",
              "num": value.cardNumber,
              "nameOnCard": value.cardName,
              "cvv": value.cvv,
              "issuedBy": "VI",
              "expiry": {
                month: +value.month,
                year: +value.year
              },
              "contactInfo": {
                "phones": [
                  {
                    "type": "Mobile",
                    "num": +this.state.phone,
                    "countryCode": "1",
                    "ext": "123"
                  }
                ],
                "billingAddress": {
                  line1: value.address,
                  city: {
                    code: "SFO",
                    name: value.city
                  },
                  state: {
                    code: this.state.region,
                    name: this.state.region
                  },
                  "countryCode": this.state.country,
                  "postalCode": value.zipcode
                },
                email: value.email,
              }
            }
          ]
        },
        "customer": {
          "name": {
            first: value.firstName,
            last: value.lastName
          },
          "contactInfo": {
            "address": {
              line1: value.address,
              city: {
                code: "SFO",
                name: value.city
              },
              state: {
                code: this.state.region,
                name: this.state.region
              },
              "countryCode": this.state.country,
              "postalCode": value.zipcode
            },
            email: value.email,
            "phones": [
              {
                "type": "Mobile",
                "num": +this.state.phone,
                "countryCode": "1",
                "ext": "123"
              }
            ]
          },
          "dob": moment(this.state.dob).format('YYYY-MM-DD'),
          "nationality": this.state.nationality.value,
          "customerId": "43435"
        },
        "driverInfo": {
          "name": {
            first: value.firstName,
            last: value.lastName
          },
          "contactInfo": {
            "address": {
              line1: value.address,
              city: {
                code: "SFO",
                name: value.city
              },
              state: {
                code: this.state.region,
                name: this.state.region
              },
              "countryCode": this.state.country,
              "postalCode": value.zipcode
            },
            email: value.email,
            "phones": [
              {
                "type": "Mobile",
                "num": +this.state.phone,
                "countryCode": "1",
                "ext": "123"
              }
            ]
          },
          "dob": moment(this.state.dob).format('YYYY-MM-DD'),
          "nationality":this.state.nationality.value
        },
        carDetails: this.props.carPricedetails,
        carBookingParameters: queryString.parse(this.props.location.search)
      }]
      this.props.payment(payload)
  
    }
  };


  render() {

    // const count = this.state.countryFilter &&  this.state.country && _filter(this.state.countryFilter,(each,i)=>{
    //      return each.name === this.state.country ;
    //  });

    const {
      handleSubmit,
      paymentDetails,
      carPaymentInfo
    } = this.props;


    let response;
    //{paymentDetails && paymentDetails.data ? paymentDetails.data: ""}

    if (paymentDetails && paymentDetails.data === "") {
      response = ""
    } else if (paymentDetails && paymentDetails.message === "success") {
      response = paymentDetails.data;
    } else if (paymentDetails && paymentDetails.message === "failure") {
      response = paymentDetails.data.message;
    }



    const { country, region, isdivHide, isModalOpen } = this.state;
    const renderModel = isModalOpen && <CompleteBookingLogin paymentInfo={carPaymentInfo} isdivHide={isdivHide} onHide={this.close} />

    var years = [],
    year = new Date().getFullYear();

  for (var i = year; i < year + 15; i++) {
    years.push(i);
  }
    return (
      <React.Fragment>
        {renderModel}

        
          <form
            onSubmit={handleSubmit(this.handleBooking)}
            style={{ width: "100%" }}
          >
            <div>
              <div className="headerTitles paymentRes justify-content-start">
                <h5>Pay with Credit Card / Debit Card</h5>
                <img src={img_paymentCard} className="cardImg" alt="" />
              </div>
              <div className="paymentDetails">
                <div className="row">
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    {this.props.loginDetails && <div className="form-group">
                      <label>Select your payment method</label>

                      <select onChange={this.handleCardSelect}>
                        {this.state.card && this.state.card.length == 0 && (
                          <option value="">No saved Cards</option>
                        )}
                        {this.state.card  && this.state.card.length > 0 && (
                          <option value="">Select your card</option>
                        )}
                        {this.state.card  && this.state.card.map((value, index) => {
                          return (
                            <option value={index}>Car ending with {value.last4}</option>
                          )
                        })
                        }
                      </select>
                    </div>}
                  </div>
                </div>
                <div className="row">
                  <div className="col-xl-9 col-lg-9 col-md-9">
                    <div className="form-group">
                      <Field
                        name="cardNumber"
                        disabled={this.state.onCardSelected}
                        type="text"
                        label="Credit Card Number"
                        component={InputField}
                        placeholder="Enter Your Card Number "
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <Field
                        name="cvv"
                        type="password"
                        label="CVV"
                        component={InputField}
                        placeholder="CVV "
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xl-6col-lg-6 col-md-6">
                    <div className="form-group">
                      <Field
                        name="cardName"
                        type="text"
                        label="Name on Card"
                        disabled={this.state.onCardSelected}
                        component={InputField}
                        onChange={(e) => this.setState({ cardName: e.target.value },
                          () => {
                            if (this.state.sameAsCreditCard) {
                              this.props.change('firstName', e.target.value)
                            }
                          }
                        )}
                        placeholder="Enter Your Name of card "
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <Field
                        name="month"
                        type="text"
                        label="Expired month"
                        component={SelectField}
                        disabled={this.state.onCardSelected}
                        placeholder="Expired month">
                        <option>Select the Month</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                      </Field>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      {/* <Field
                        name="year"
                        type="text"
                        label="Year"
                        disabled={this.state.onCardSelected}
                        component={InputField}
                        placeholder="year"
                      /> */}

                      <Field
                      name="year"
                      type="text"
                      component={SelectField}
                      placeholder="YYYY"
                      className="form-control selectCard"
                      disabled={this.state.onCardSelected}
                    ><option value="" disabled>YYYY</option>
                      {years.map((each, i) => (
                        <option value={each}>{each}</option>
                      ))}
                    </Field>
                    </div>
                  </div>
                </div>
                <h5>Billing Address</h5>
                <div className="row">
                  <div className="col-xl-9 col-lg-9 col-md-9">
                    <div className="form-group">
                      <Field
                        name="address"
                        type="text"
                        label="Address"
                        component={InputField}
                        placeholder="Address "
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <label>Country</label>
                      {/* <CountryDropdown
                      name={country}
                      value={country}
                      valueType="short"
                      countryValueType="short"
                      onChange={(val,e) => console.log("err",e.target.value)}
                    /> */}
                      <CountryDropdown
                        name={country}
                        value={country}
                        valueType="short"
                        onChange={val => this.selectCountry(val)}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xl-5 col-lg-5 col-md-5">
                    <div className="form-group">
                      <Field
                        name="city"
                        type="text"
                        label="City/Town"
                        component={InputField}
                        placeholder="City/Town "
                      />
                    </div>
                  </div>
                  <div className="col-xl-4 col-lg-4 col-md-4">
                    <div className="form-group">
                      <label>State</label>
                      <RegionDropdown
                        country={country}
                        value={region}
                        countryValueType="short"
                        valueType="short"
                        onChange={val => this.selectRegion(val)}
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <Field
                        name="zipcode"
                        type="text"
                        label="Zip Code"
                        component={InputField}
                        placeholder="Zip Code"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xl-5 col-lg-5 col-md-5">
                    <div className="form-group">
                      <Field
                        name="firstName"
                        type="text"
                        label="Who is driving..?"
                        component={InputField}
                        disabled={this.state.firstNameDisabled}
                        placeholder="First Name"
                      />
                    </div>
                  </div>
                  <div className="col-xl-4 col-lg-4 col-md-4">
                    <div className="form-group">
                      <Field
                        name="lastName"
                        type="text"
                        component={InputField}
                        placeholder="Last Name "
                      />
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <label>Same as Credit Card</label>
                      <input
                        className="styled-checkbox"
                        id="styled-checkbox-1"
                        type="checkbox"
                        onChange={
                          () => {
                            this.setState({ sameAsCreditCard: !this.state.sameAsCreditCard, firstNameDisabled: !this.state.firstNameDisabled }, () => {
                              if (this.state.sameAsCreditCard) {
                                this.props.change('firstName', this.state.cardName)
                              }
                            })
                          }
                        }
                        value={this.state.sameAsCreditCard}
                      />
                      <label htmlFor="styled-checkbox-1" />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xl-5 col-lg-5 col-md-5">
                    <div className="form-group">
                      <Field
                        name="email"
                        type="text"
                        label="Email"
                        component={InputField}
                        placeholder="Email"
                      />
                    </div>
                  </div>
                  <div className="col-xl-4 col-lg-4 col-md-4">
                    <div className="form-group">
                      <label>Phone Number</label>
                      <PhoneInput
                        placeholder="Enter Mobile Number"
                        name="contact"
                        value={this.state.phone}
                        onChange={phone =>
                          this.setState({ phone }, () => {
                            if (phone) {
                              if (isValidPhoneNumber(phone)) {
                                this.setState({
                                  concatError: "",
                                  contactValid: true
                                });
                              } else {
                                this.setState({
                                  concatError: "Please enter valid Mobile Number",
                                  contactValid: false
                                });
                              }
                            }
                          })
                        }
                      />

                      {!this.state.contactValid && (
                        <span style={{ color: "red" }}>
                          {this.state.concatError}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <label>Nationality</label>
                    <Select
                      options={this.state.options}
                      value={this.state.nationality}
                      onChange={this.changeHandlerNationality}
                    />
                  </div>
                </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <label>Date of Birth</label>
                      
                      <DatePicker
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      placeholder="YYYY/MM/DD"
                      maxDate={moment().subtract(17,'years').year().toString()}
                      />
                      {/* <input value={moment(this.state.dob).format('MM/DD/YYYY')} onFocus={() => this.setState({ isDob: true })} />
                      {this.state.isDob &&
                      <div onMouseLeave={() => {this.setState({isDob:false})}}>
                         <DateRangePicker
                        selectionType="single"
                        value={this.state.dob}
                        initialYear='1990'
                        onSelect={(dob) => {
                          this.setState({
                            dob,
                            isDob: false
                          })
                        }}
                      /></div>} */}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12 mt-3 mb-1 text-right">
                    {/* <button type="button" className="searchBtn">
                      Add to itinerray
                  </button> */}
                    <button className="searchBtn completebtn">
                      Complete Booking
                  </button>
                  </div>
                </div>
              </div>
            </div>
          </form>


        {this.state.isModal === true && (
          <div
            className="modal backgroundDark"
            id="myModal"
            style={{ display: "block" }}
          >
            <div className="modal-dialog signInPopup">
              <div className="modal-content">
                <div className="modal-body paymentError">
                  <div className="socialBtnGroup" />
                  <span className="erroricon">
                    <i class="fas fa-exclamation-triangle" />
                  </span>
                  <h4>
                    {paymentDetails && paymentDetails.hasOwnProperty('message')
                      ? paymentDetails.message
                      : "Loading...."}

                  </h4>

                  <button
                    type="button"
                    className="goBack"
                    onClick={this.handleModal}
                  >
                    Go Back
                  </button>
                </div>
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const fieldValidation = formProps => {

  const errors = {};

  if (!formProps.cardNumber) {
    errors.cardNumber = "Required";
  } else if (/\D/.test(formProps.cardNumber)) {
    errors.cardNumber = "numbers only allowed";
  }
  if (!formProps.cardName) {
    errors.cardName = "Required";
  }
  if (!formProps.cvv) {
    errors.cvv = "Required";
  } else if (!/^[0-9]{3,4}$/.test(formProps.cvv)) {
    errors.cvv = "cvv should contain atleast 3 digit";
  }
  if (!formProps.month) {
    errors.month = "Required";
  }
  if (formProps.year != "" && !/^[0-9]+$/.test(formProps.year)) {
    errors.year = "Please Enter Numeric Values Only";
  } else if (!/^[0-9]{4}$/i.test(formProps.year)) {
    errors.year = "year should contain 4 digit";
  }
  if (!formProps.address) {
    errors.address = "Required";
  }
  if (!formProps.city) {
    errors.city = "Required";
  }
  if (!formProps.state) {
    errors.state = "Required";
  }
  if (!formProps.country) {
    errors.country = "Required";
  }
  if (!formProps.zipcode) {
    errors.zipcode = "Required";
  } else if (/\D/.test(formProps.zipcode)) {
    errors.zipcode = "zipcode allowed only number";
  }
  if (!formProps.firstName) {
    errors.firstName = "First name is Required";
  }else if(formProps.firstName.includes('.')){
    errors.firstName = "Please enter valid first name";
  }

  if (!formProps.lastName) {
    errors.lastName = "Last name is required";
  }else if(formProps.lastName.includes('.')){
    errors.lastName = "Please enter valid last name";
  }

  if (!formProps.email) {
    errors.email = "Required";
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formProps.email)
  ) {
    errors.email = "Invalid Email Address";
  }
  if (!formProps.code) {
    errors.code = "Required";
  }
  if (!formProps.phoneNumber) {
    errors.phoneNumber = "Required";
  }

  return errors;
};

const mapStateToProps = state => ({
  sessionId: state.carReducer.sessionId,
  carPricedetails: state.carReducer.carPrice,
  loginDetails: state.loginReducer.loginDetails,
  paymentDetails: state.carReducer.carBookingResult,
  selectedCurrency: state.commonReducer.selectedCurrency,
  gustLoginCar :state.loginReducer.gustLoginCar,
  carPaymentInfo:state.carReducer.carPaymentInfo,
  loginStatus: state.loginReducer.loginStatus,
});

const mapDispatchToProps = dispatch => ({
  payment: payload => dispatch(carBooking(payload)),
  getCard: email => dispatch(getCard(email)),
  deletePaymentProps: dispatch(deletePaymentProps(null)),
  signupReset : () => dispatch(signupReset()),
 
  carGetPayload: (payload) => dispatch(carGetPayload(payload)),
  bookingSignUpInfo : (guestFlag ,payload ,payloadInfo) => dispatch(bookingSignUpInfo(guestFlag ,payload ,payloadInfo)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(
  reduxForm({
    form: "paymentCar",
    validate: fieldValidation
  })(CarPayment)
));

CarPayment.propTypes = {
  isdivHide: propTypes.bool
}
CarPayment.defaultProps = {
  loginDetails: {}
}