import React, { Component } from "react";
import { connect } from "react-redux";
import { reduxForm, Field } from "redux-form";
import propTypes from "prop-types";
import { withRouter } from "react-router-dom";

import moment from "moment";
import PhoneInput from "react-phone-number-input";
import { isValidPhoneNumber } from "react-phone-number-input";
import { CountryDropdown, RegionDropdown } from "react-country-region-selector";
import queryString from "query-string";

import "react-phone-number-input/style.css";
import Calendar from 'react-calendar';
import DateRangePicker from "react-daterange-picker";
import originalMoment from "moment";
import { extendMoment } from "moment-range";
import "react-daterange-picker/dist/css/react-calendar.css";
import Select from 'react-select'
import countryList from 'react-select-country-list'

import {
  map as _map,
  pick as _pick,
  partialRight as _partialRight,
  filter as _filter
} from "lodash";

import { getCard } from "../../../service/card/action";
import { payment, bookingReset } from "../../../service/payment/action";
import { dumpValue,replaceItinerary } from "../../../service/addCart/action";


import InputField from "../../Fields/TextField";
import SelectField from "../../Fields/SelectField";
import CheckField from '../../Fields/CheckField';

import SignInModal from "../../container/login/SignInModal";

import img_paymentCard from "../../../asset/images/paymentCard1.png";
import { FLUSH } from "redux-persist";

import MultipleContext from './context'

import axios from "../../../service/Axios"

import URL from '../../../asset/configUrl';

import { loadingGifSearch,stopGifSearching } from "../../../service/common/action"
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


import { loginInfo } from "../../../service/login/action"

import Notifications, { notify } from "react-notify-toast";

class MulipleBookingPaymentView extends Component {
  state = {
    isModal: false,
    isOpen: "",
    card: [],
    phone: "",
    concatError: "",
    contactValid: true,
    country: "",
    region: "",
    countryFilter: "",
    isdivHide: true,
    isModalOpen: false,
    selected: { id: "" },
    btnDisable: false,
    isCardInfoDisable: false,
    dob: moment().subtract('years', 18),
    options: countryList().getData(),
    nationality: '',
    sameAsCreditCard: false,
    firstNameDisabled: false,
    cardName: '',

    allBookingPayloadCar:[],
    allBookingPayloadHotel:[],
    allBookingPaymentDetails:[],
    bookingErrorHotel:"",
    bookingErrorCar:"",
    startDate:moment().subtract(17,'years').toDate(),
    email:""
  };
 handleChange = date => {
    this.setState({
      startDate: date
    });
  };

   get2D =( num ) => {
    return ( num.toString().length < 2 ? "0"+num : num ).toString();
   }

  handleFormInitialValues = (index) => {
    this.props.change("cardName", "" || (this.state.selected && this.state.selected.name))
    this.props.change("cardNumber", "" || (this.state.selected && this.state.selected.last4))
    this.props.change("cvv", "")
    this.props.change("month", "" || (this.state.selected && this.get2D(this.state.selected.exp_month)))
    this.props.change("year", "" || (this.state.selected && this.state.selected.exp_year))
  };

  changeHandler = nationality => {
    this.setState({ nationality })
  }

  getCard = (nextProps) => {
    let config = {
      headers: {
        "secret-code": "xeni-app-development"
      }
    };
    if (nextProps) {
      const { email } = nextProps;
      axios.get(
        URL.card.CARD_GET + email,
        config
      )
        .then(response => {
          this.setState({ card: response.data.data.data });
        });
    }
  };

  componentWillMount = () => {
    this.getCard();
    this.props.bookingReset();
  }

  handleSelectCard = e => {
    this.setState({ card: e.target.value });
  };

  handleCardSelect = e => {
  
    if (e.target.value) {
      this.setState(
        {
          selected: this.state.card[e.target.value],
          isCardInfoDisable: true
        },
        () => this.handleFormInitialValues()
      );
    } else {
      this.setState({
        isCardInfoDisable: false,
        selected: { id: "" }
      })
      this.props.change("cardName", "")
      this.props.change("cardNumber", "")
      this.props.change("cvv", "")
      this.props.change("month", "")
      this.props.change("year", "")

    }
  };

  // componentWillReceiveProps(nextProps) {
  //   if (this.props.bookingConfirm !== nextProps.bookingConfirm) {
  //     if (nextProps.bookingConfirm === true) {
  //       this.props.history.push("/bookingconfirmation");
  //     } else if (nextProps.bookingConfirm === false) {
  //       this.setState({ isModal: true });
  //     }

  //   }

  //paymentFailureDetails.data.message
  // }


  componentWillReceiveProps(nextProps) {

    const { bookingConfirm, paymentDetails, loginDetails, paymentFailureDetails } = nextProps;

    if (nextProps.loginDetails) {
      this.getCard(nextProps.loginDetails);
    } else {
      this.setState({ card: null })
    }

    if (bookingConfirm === 'success') {
      //this.props.history.push("/bookingconfirmation");
    }
    if (bookingConfirm === 'failure') {
      this.setState({ isModal: true })
    }
    // else if(nextProps.loginDetails === null || nextProps.loginDetails.hasOwnProperty('status') && nextProps.loginDetails.status == false){
    //   this.setState({ isModalOpen: true });

    // }else if(nextProps.paymentFailureDetails.data.message === "please provide valid email"){
    //   this.setState({ isModalOpen: true });
    // }
  }


  selectCountry(val) {
    if (val !== "" && this.state.region !== "" && this.state.phone !== "") {
      this.setState({ country: val, btnDisable: true });
    } else {
      this.setState({ country: val, btnDisable: false });
    }
  }

  selectRegion(val) {
    if (val !== "" && this.state.country !== "" && this.state.phone !== "") {
      this.setState({ region: val, btnDisable: true });
    } else {
      this.setState({ region: val, btnDisable: false });
    }
  }

  handleModal = () => {
    this.props.bookingReset();
    this.setState({ isModal: !this.state.isModal });

  };

  handleSelectCard = e => {
    this.setState({ card: e.target.value });
  };

  close = () => {
    this.setState({ isModalOpen: false });
  };


  generatePassword = () =>  {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }

  bookingFinalCheckout=async(value,email)=>{

      const {
        hotel,
        sessionId,
        tripAmount,
        periodStay,
        roomPriceInfo,
        checkin,
        checkout,
        searchDate
      } = this.props;


      let cardId=this.state.selected.id

      console.log("card id=======================",cardId)
      // if(this.state.card && this.state.card.length !=0){
      //  cardId = this.state.card[0].id 
      // }else{
      //  cardId=null
      // }

      
      let payloadHotelArray=[].concat.apply([],this.state.allBookingPayloadHotel)

      const hotelPayload = _map(payloadHotelArray, (each, i) => {
        console.log("hotel data",each)
        return {
          images: [each.hotel.images[0], each.hotel.images[1]],
          policies: each.hotel.policies,
          checkinCheckoutPolicy: each.hotel.checkinCheckoutPolicy,
          amenities: [
            each.hotel.amenities[0],
            each.hotel.amenities[1],
            each.hotel.amenities[2],
            each.hotel.amenities[3],
            each.hotel.amenities[4]
          ],
          rooms_info: each.rooms,
          stayPeriod: {
            start: each.checkin,
            end: each.checkout
          },
          rating: each.hotel.rating,
          geocode: each.hotel.geocode,
          sessionId: each.sessionId,
          email: email,
          hotelId: each.hotel.id,
          rooms: [
            {
              roomRefId: each.pricedRooms[0].roomRefId,
              rateRefId: each.pricedRooms[0].rateRefId,
              guests: [
                {
                  type: "Adult",
                  name: {
                    first: value.firstName,
                    last: value.lastName
                  },
                  age: 25
                }
              ]
            }
          ],
          totalAmount: each.totalAmount,
          amtAfterdiscount: each.amtAfterdiscount,
          farebreakupAmount: each.farebreakupAmount,
          paymentBreakup: [
            // TODO : Ask jansi to clarify hard coded
            {
              paymentMethodRefId: "1",
              amount: each.fareBreakup.totalFare,
              currency:  each.currency,
              type: "Cash"
            }
          ],
          paymentMethod: {
            cards: [
              {
                num: value.cardNumber,
                nameOnCard: value.cardName,
                cvv: value.cvv,
                issuedBy: "VI",
                expiry: {
                  month: value.month,
                  year: value.year
                },
                contactInfo: {
                  phones: [
                    {
                      num: value.phoneNumber
                    }
                  ],
                  billingAddress: {
                    line1: value.address,
                    city: {
                      code: "SFO",
                      name: value.city
                    },
                    state: {
                      code: this.state.region,
                      name: this.state.region
                    },
                    countryCode: this.state.country,
                    postalCode: value.zipcode
                  },
                  email: value.email
                }
              }
            ],
            card_id:  cardId
            //  card_id : this.state.card[0] && this.state.card[0].id || null
            //card_id: (this.state.selected && this.state.selected[0].id) || null
          },
          customer: {
            name: {
              first: value.firstName,
              last: value.lastName
            },
            contactInfo: {
              phones: [
                {
                  num: this.state.phone
                }
              ],
              address: {
                line1: value.address,
                city: {
                  code: "SFO",
                  name: value.city
                },
                state: {
                  code: this.state.region,
                  name: this.state.region
                },
                countryCode: this.state.country,
                postalCode: value.zipcode
              },
              email: value.email
            },
            dob: this.state.dob,
            nationality: this.state.nationality.value,
            customerId: "43435" // TODO : We are using mail id to retrive data so now just pass dummy value
          },
          primaryGuest: {
            name: {
              first: value.firstName,
              last: value.lastName
            },
            contactInfo: {
              phones: [
                {
                  num: this.state.phone
                }
              ],
              address: {
                line1: value.address,
                city: {
                  code: "SFO",
                  name: value.city
                },
                state: {
                  code: this.state.region,
                  name: this.state.region
                },
                countryCode: this.state.country,
                postalCode: value.zipcode
              },
              email: value.email
            },
            age: 25
          },
          booking_type: "Hotel",
          date_booked: moment(new Date()).format("MM/DD/YYYY"),
          travel_date: each.checkin,
          hotel_address: {
            name: each.hotel.name,
            contact: each.hotel.contact
          },
          cancellationPolicy: each.rates[0].cancellationPolicy,
          refundability: each.rates[0].refundability,
          fareBreakup: each.fareBreakup,
          guestInfo: {
            // adultCount: queryValues.adult,
            // childCount: queryValues.child

            adultCount: 1,
            childCount: 1
          }
        };
      });

    //  console.log("payment payload", payload);
    
      let carPayLoad= this.state.allBookingPayloadCar.map((carPricedetails)=>{
        console.log("car data",carPricedetails)
       return {
          sessionId: carPricedetails.sessionId,
          email,
          rentalId: carPricedetails.carRental.id,
          "paymentBreakup": [
            {
              // "paymentMethodRefId": "ea99994f-fdda-465c-9081-e65b7092b99f",
              "amount": +carPricedetails.quotedTotalFare,
              "currency": carPricedetails.currency,
              "type": "Cash"
            }
          ],
          "actualPaymentBreakup": [
            {
              "amount": +carPricedetails.pricedTotalFare,
              "currency": carPricedetails.currency,
              "type": "Cash"
            }
          ],
          "paymentMethod": {
            "card_id": cardId,
            "cards": [
              {
                "refId": "ea99994f-fdda-465c-9081-e65b7092b99f",
                "num": value.cardNumber,
                "nameOnCard": value.cardName,
                "cvv": value.cvv,
                "issuedBy": "VI",
                "expiry": {
                  month: +value.month,
                  year: +value.year
                },
                "contactInfo": {
                  "phones": [
                    {
                      "type": "Mobile",
                      "num": +this.state.phone,
                      "countryCode": "1",
                      "ext": "123"
                    }
                  ],
                  "billingAddress": {
                    line1: value.address,
                    city: {
                      code: "SFO",
                      name: value.city
                    },
                    state: {
                      code: this.state.region,
                      name: this.state.region
                    },
                    countryCode: this.state.country,
                    "postalCode": value.zipcode
                  },
                  email: value.email,
                }
              }
            ]
          },
          "customer": {
            "name": {
              first: value.firstNameCar,
              last: value.lastNameCar
            },
            "contactInfo": {
              "address": {
                line1: value.address,
                city: {
                  code: "SFO",
                  name: value.city
                },
                state: {
                  code: this.state.region,
                  name: this.state.region
                },
                countryCode: this.state.country,
                "postalCode": value.zipcode
              },
              email: value.email,
              "phones": [
                {
                  "type": "Mobile",
                  "num": +this.state.phone,
                  "countryCode": "1",
                  "ext": "123"
                }
              ]
            },
            "dob": moment(this.state.dob).format('YYYY-MM-DD'),
            "nationality": this.state.nationality.value,
            "customerId": "43435"
          },
          "driverInfo": {
            "name": {
              first: value.firstNameCar,
              last: value.lastNameCar
            },
            "contactInfo": {
              "address": {
                line1: value.address,
                city: {
                  code: "SFO",
                  name: value.city
                },
                state: {
                  code: this.state.region,
                  name: this.state.region
                },
                countryCode: this.state.country,
                "postalCode": value.zipcode
              },
              email: value.email,
              "phones": [
                {
                  "type": "Mobile",
                  "num": +this.state.phone,
                  "countryCode": "1",
                  "ext": "123"
                }
              ]
            },
            "dob": moment(this.state.dob).format('YYYY-MM-DD'),
            "nationality":this.state.nationality.value
          },
          carDetails: carPricedetails,
          carBookingParameters: carPricedetails.bookingParameters
        }
      })

      let allBookingPromise=[]
      this.props.loadingGifSearch()

      if(hotelPayload.length !=0){
        await axios.post(URL.hotel.ROOM_BOOKING,hotelPayload).then((data)=>{
          allBookingPromise.push(data)
          this.setState({ bookingErrorHotel:"",bookingErrorCar:"" });
        })
        .catch((err)=>{
          allBookingPromise.push({error:err.response.data})
        })
      }else{
        allBookingPromise.push(null)
        this.setState({ bookingErrorHotel:"",bookingErrorCar:"" });
      }

      if(carPayLoad.length !=0){
        await axios.post(URL.CAR_BOOKING,carPayLoad).then((data)=>{
          if(data.data.hasOwnProperty("error")){
            allBookingPromise.push({error:data.data})
          }else{
            allBookingPromise.push(data)
            this.setState({ bookingErrorHotel:"",bookingErrorCar:"" });
          }
        })
        .catch((err)=>{
          allBookingPromise.push({error:err.response.data})
        })
      }
      else{
        allBookingPromise.push(null)
        this.setState({ bookingErrorHotel:"",bookingErrorCar:"" });
      }

      await allBookingPromise.map(async (data,index)=>{
        this.props.stopGifSearching()
        if(data && data.hasOwnProperty("error") && index == 0){
          await this.setState({ isModal: true,bookingErrorCar:"" });
          this.setState({ isModal: true,bookingErrorHotel:data.error.data.message });
        }
        else if(data && data.hasOwnProperty("error") && index == 1){
          if(this.state.bookingErrorHotel == data.error.message){
           await this.setState({ isModal: true,bookingErrorCar:"" });
          }else{
           await this.setState({ isModal: true,bookingErrorHotel:data.error.message });
          }
        }
      })
      
       if(this.state.bookingErrorHotel != "Please provide valid card details")
       {
           let successData=[]
      allBookingPromise.map((responseData)=>{
        if(responseData && responseData.hasOwnProperty("data") || responseData == null){
          successData.push(responseData)
        }else if(responseData && responseData.hasOwnProperty("error")){
          successData.push(responseData)
        }else{
          successData.push(null)
        }
      })
        this.props.dumpValue(successData)
        this.props.replaceItinerary()
        this.props.history.push("/multipleconfirmation")
       }
  }

  handleBooking =async (value) => {

    this.props.loadingGifSearch()
    
    if (this.props.loginStatus === false || this.props.loginStatus === undefined) {

      let passowrdGenerated=this.generatePassword()
      const payload = {
        name: value.firstName || value.firstNameCar,
        email: value.email ,
        password: passowrdGenerated,
        type:"guest"
      };

      let userStatusResponse
      await axios.post(URL.USER_SIGNUP,payload).then((data)=>{
       userStatusResponse=data.data
      }).catch((err)=>{
        userStatusResponse=err.response.data
      })

      if(userStatusResponse.status == true){
       await axios.post(URL.USER_LOGIN,{email:value.email,password:passowrdGenerated}).then((data)=>{
        sessionStorage.setItem("loginInfo", JSON.stringify(data.data.data));
        sessionStorage.setItem("loginTrue" , JSON.stringify("true"))
         this.props.dispatch({
            type: "LOGIN_SUCCESS",
            payload: data.data.data
         });
         notify.show("Thanks for Registering with Us , Welcome to Xeniapp .Logged in successfully" ,"success", 3000);
       }).catch((err)=>{
        // userStatusResponse=err.response.data
       })
        this.bookingFinalCheckout(value,value.email)
      }
      else if(userStatusResponse.status == false && userStatusResponse.data.includes("Email already exists!")){
         this.props.stopGifSearching()
         this.setState({
           email:value.email
         },()=>{
          this.setState({ isModalOpen: true });
         })
         this.setState({ isModal: false });
      }
    }
    else{
      const { email } = this.props.loginDetails && this.props.loginDetails;
      this.bookingFinalCheckout(value,email)
    }
  };

  changeCardInfo = () => {
    console.log('changeCardInfo')
  }
  componentDidMount() {
    axios.get("https://restcountries.eu/rest/v2/all").then(response => {
      this.setState({
        countryFilter: _map(
          response.data,
          _partialRight(_pick, ["name", "alpha2Code"])
        )
      });
    });
  }

  render() {
    const { handleSubmit, paymentFailureDetails } = this.props;
    const { country, region, isdivHide, isModalOpen, isDob, firstNameDisabled } = this.state;
    const renderModel = isModalOpen && (
      <SignInModal emailAuto={this.state.email} isdivHide={isdivHide} onHide={this.close} />
    );
    var years = [],
      year = new Date().getFullYear();

    for (var i = year; i < year + 15; i++) {
      years.push(i);
    }
    let {allBookingPayloadCar,allBookingPayloadHotel}= this.props.context
    console.log(allBookingPayloadCar,allBookingPayloadHotel)
    return(
      <React.Fragment>
        {renderModel}
        <form
          onSubmit={handleSubmit(this.handleBooking)}
          onClick={()=>{
              this.setState({
                  allBookingPayloadCar,allBookingPayloadHotel
              })
          }}
          style={{ width: "100%" }}
        >
          <div>
            <div className="headerTitles paymentRes justify-content-start">
              <h5>Pay with Credit Card / Debit Card</h5>
              <img src={img_paymentCard} className="cardImg" alt="" />
            </div>
            <div className="paymentDetails">
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  {this.props.loginStatus && <div className="form-group">
                    <label>Select your payment method</label>

                    <select onChange={this.handleCardSelect} value={this.state.cardInfo}>
                      {this.state.card && this.state.card.length == 0 && (
                        <option value="">No saved Cards</option>
                      )}
                      {this.state.card && this.state.card.length > 0 && (
                        <option value="">select your card</option>
                      )}
                      {this.state.card &&
                        this.state.card.length >= 0 &&
                        _map(this.state.card, (each, i) => (
                          <option value={i}>
                            Card ending in {each.last4}
                          </option>
                        ))}
                    </select>
                  </div>}
                </div>
              </div>
              <div className="row">
                <div className="col-xl-9 col-lg-9 col-md-9">
                  <div className="form-group">
                    <Field
                      name="cardNumber"
                      type="text"
                      label="Credit Card Number"
                      component={InputField}
                      placeholder="Enter Your Card Number "
                      disabled={this.state.isCardInfoDisable}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <Field
                      name="cvv"
                      type="password"
                      label="CVV"
                      component={InputField}
                      placeholder="CVV "
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-6col-lg-6 col-md-6">
                  <div className="form-group">
                    <Field
                      name="cardName"
                      type="text"
                      label="Name on Card"
                      component={InputField}
                      placeholder="Enter Your Name of card "
                      disabled={this.state.isCardInfoDisable}
                      onChange={(e) => this.setState({ cardName: e.target.value },
                        () => {
                          if (this.state.sameAsCreditCard) {
                            this.props.change('firstName', e.target.value)
                          }
                        }
                      )}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <Field
                      name="month"
                      label="Vaild Thru"
                      component={SelectField}
                      placeholder="MM"
                      disabled={this.state.isCardInfoDisable}
                      className="form-control selectCard borderRight"
                    >
                      <option value="" disabled>MM</option>
                      <option value="01">01</option>
                      <option value="02">02</option>
                      <option value="03">03</option>
                      <option value="04">04</option>
                      <option value="05">05</option>
                      <option value="06">06</option>
                      <option value="07">07</option>
                      <option value="08">08</option>
                      <option value="09">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </Field>
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <Field
                      name="year"
                      type="text"
                      component={SelectField}
                      placeholder="YYYY"
                      className="form-control selectCard"
                      disabled={this.state.isCardInfoDisable}
                    ><option value="" disabled>YYYY</option>
                      {years.map((each, i) => (
                        <option value={each}>{each}</option>
                      ))}
                    </Field>
                  </div>
                </div>
              </div>
              <h5>Billing Address</h5>
              <div className="row">
                <div className="col-xl-9 col-lg-9 col-md-9">
                  <div className="form-group">
                    <Field
                      name="address"
                      type="text"
                      label="Address"
                      component={InputField}
                      placeholder="Address "
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <label>Select Country</label>
                    <CountryDropdown
                      name={country}
                      value={country}
                      valueType="short"
                      onChange={val => this.selectCountry(val)}
                    />
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <Field
                      name="city"
                      type="text"
                      label="City/Town"
                      component={InputField}
                      placeholder="City/Town "
                    />
                  </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="form-group">
                    <label>State</label>
                    <RegionDropdown
                      country={country}
                      value={region}
                      countryValueType="short"
                      valueType="short"
                      onChange={val => this.selectRegion(val)}
                    />
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <Field
                      name="zipcode"
                      type="text"
                      label="Zip Code"
                      component={InputField}
                      placeholder="Zip Code"
                    />
                  </div>
                </div>
              </div>

              { allBookingPayloadHotel.length !=0 && <div>
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <label className="whosText">Who is Checking in?</label>
                  </div>
                </div>
              </div>
              
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <Field
                      name="firstName"
                      type="text"
                      label="First Name"
                      component={InputField}
                      placeholder="First Name"
                      disabled={firstNameDisabled}
                    />
                  </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="form-group">
                    <Field
                      name="lastName"
                      type="text"
                      label="Last Name"
                      component={InputField}
                      placeholder="Last Name"
                    />
                  </div>
                  {/* TODO : same as credit card may be require in future */}
                  {/* <div className="form-group">
                    <label>Same as Credit Card</label>
                    <input
                      className="styled-checkbox"
                      id="styled-checkbox-1"
                      type="checkbox"
                      value={this.state.sameAsCreditCard}
                      name='sameAsCreditCard'
                      onChange={
                        () => {
                          this.setState({ sameAsCreditCard: !this.state.sameAsCreditCard, firstNameDisabled: !this.state.firstNameDisabled }, () => {
                            if (this.state.sameAsCreditCard) {
                              this.props.change('firstName', this.state.cardName)
                            } else {
                              this.props.change('firstName', '');
                            }
                          })
                        }
                      }
                    />
                    <label htmlFor="styled-checkbox-1" />
                  </div> */}
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <label>Date of Birth</label>
                     <DatePicker
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      maxDate={moment().subtract(17,'years').year().toString()}
                      dropdownMode="select"
                      placeholder="YYYY/MM/DD"
                    />
                  </div>
                </div>
              </div>
              </div>}

             { allBookingPayloadCar.length !=0 && <div>
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <label className="whosText">Who is Driving?</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <Field
                      name="firstNameCar"
                      type="text"
                      label="First Name"
                      component={InputField}
                      placeholder="First Name"
                      disabled={firstNameDisabled}
                    />
                  </div>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="form-group">
                    <Field
                      name="lastNameCar"
                      type="text"
                      label="Last Name"
                      component={InputField}
                      placeholder="Last Name"
                    />
                  </div>
                  {/* TODO : same as credit card may be require in future */}
                  {/* <div className="form-group">
                    <label>Same as Credit Card</label>
                    <input
                      className="styled-checkbox"
                      id="styled-checkbox-1"
                      type="checkbox"
                      value={this.state.sameAsCreditCard}
                      name='sameAsCreditCard'
                      onChange={
                        () => {
                          this.setState({ sameAsCreditCard: !this.state.sameAsCreditCard, firstNameDisabled: !this.state.firstNameDisabled }, () => {
                            if (this.state.sameAsCreditCard) {
                              this.props.change('firstName', this.state.cardName)
                            } else {
                              this.props.change('firstName', '');
                            }
                          })
                        }
                      }
                    />
                    <label htmlFor="styled-checkbox-1" />
                  </div> */}
                </div>

       
                {/* {this.state.allBookingPayloadCar.length != 0 && this.state.allBookingPayloadHotel.length != 0 ? <div>Used</div>:<div>Tested</div>}
                 */}
                {allBookingPayloadCar.length != 0 && allBookingPayloadHotel.length != 0 ? <div></div>:<div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <label>Date of Birth</label>
                    
                    <DatePicker
                      selected={this.state.startDate}
                      onChange={this.handleChange}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      maxDate={moment().subtract(17,'years').year().toString()}
                      dropdownMode="select"
                      placeholder="YYYY/MM/DD"
                    />
                    {/* <input value={moment(this.state.dob).format('MM/DD/YYYY')} onFocus={() => this.setState({ isDob: true })} />
                    {isDob && <DateRangePicker
                      selectionType="single"
                      value={this.state.dob}
                      initialYear='1990'
                      onSelect={(dob) => {
                        this.setState({
                          dob,
                          isDob: false
                        })
                      }}
                    />} */}
                  </div>
                </div>}
              </div>
              </div>}
              {/* <p className="whosText"></p> */}

              {/* <div className="row">
															
                <div className="col-xl-9 col-lg-9 col-md-9">
                    <div className="form-group">
                      <label className="whosText">Who is Checking in?</label>
                      <input type="text" placeholder="First and Last Name"/>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3">
                    <div className="form-group">
                      <label>Same as Credit Card</label>	
                      <input className="styled-checkbox" id="styled-checkbox-1" type="checkbox" value=""/>
                      <label for="styled-checkbox-1"></label>
                    </div>
                  </div>
							</div> */}
              <div className="row">
                <div className="col-xl-5 col-lg-5 col-md-5">
                  <div className="form-group">
                    <Field
                      name="email"
                      type="text"
                      label="Email"
                      component={InputField}
                      placeholder="Email"
                    />
                  </div>
                </div>

                <div className="col-xl-4 col-lg-4 col-md-4">
                  <div className="form-group">
                    <label>Phone Number</label>
                    <PhoneInput
                      placeholder="Mobile Number"
                      name="contact"
                      value={this.state.phone}
                      onChange={phone =>
                        this.setState({ phone }, () => {
                          if (
                            phone !== undefined &&
                            this.state.country !== "" &&
                            this.state.region !== ""
                          ) {
                            if (isValidPhoneNumber(phone)) {
                              this.setState({
                                concatError: "",
                                contactValid: true,
                                btnDisable: true
                              });
                            } else {
                              this.setState({
                                concatError:
                                  "Please enter valid Mobile Number",
                                contactValid: false,
                                btnDisable: true
                              });
                            }
                          } else {
                            this.setState({ btnDisable: false });
                          }
                        })
                      }
                    />

                    {!this.state.contactValid && (
                      <span style={{ color: "red" }}>
                        {this.state.concatError}
                      </span>
                    )}
                  </div>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-3">
                  <div className="form-group">
                    <label>Nationality</label>
                    <Select
                      options={this.state.options}
                      value={this.state.nationality}
                      onChange={this.changeHandler}
                    />
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-12 mt-3 mb-1 text-right">
                  {/* <button type="button" className="searchBtn">
                    Add to itinerray
                  </button> */}
                  <button
                    disabled={!this.state.btnDisable || !this.state.nationality}
                    className="searchBtn completebtn"
                  >
                    Complete Booking
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
        {
          this.state.isModal === true && (
            <div
              className="modal backgroundDark"
              id="myModal"
              style={{ display: "block" }}
            >
              <div className="modal-dialog signInPopup">
                <div className="modal-content">
                  <div className="modal-body paymentError">
                    <div className="socialBtnGroup" />
                    <span className="erroricon">
                      <i class="fas fa-exclamation-triangle" />
                    </span>
                    <h4>
                       {this.state.bookingErrorHotel}
                    </h4>
                    <br/>
                    {/* <h4>
                       {this.state.bookingErrorCar}
                    </h4> */}
                    <button
                      type="button"
                      className="goBack"
                      onClick={this.handleModal}
                    >
                      Go Back
                  </button>
                  </div>
                </div>
              </div>
            </div>
          )
        }
      </React.Fragment >
    
    );
  }
}

const fieldValidation = formProps => {
  const errors = {};

  if (!formProps.cardNumber) {
    errors.cardNumber = "Required";
  } else if (/\D/.test(formProps.cardNumber)) {
    errors.cardNumber = "Numbers only allowed";
  }
  if (!formProps.cardName) {
    errors.cardName = "Required";
  }
  if (!formProps.cvv) {
    errors.cvv = "Required";
  } else if (!/^[0-9]{3,4}$/.test(formProps.cvv)) {
    errors.cvv = "Cvv should contain 3-4 digit";
  }
  if (!formProps.month) {
    errors.month = "Required";
  } else if (!/^[0-9]{2}$/.test(formProps.month)) {
    errors.month = "Month should be 01-12";
  } else if (formProps.month > 12) {
    errors.month = "Month should be 01-12"
  } else if (formProps.month === "00") {
    errors.month = "Month should be 01-12"
  }

  if (formProps.year != "" && !/^[0-9]+$/.test(formProps.year)) {
    errors.year = "Please Enter Numeric Values Only";
  } else if (!/^[0-9]{4}$/i.test(formProps.year)) {
    errors.year = "Year should contain 4 digit";
  }
  if (!formProps.address) {
    errors.address = "Required";
  }
  if (!formProps.city) {
    errors.city = "Required";
  }
  if (!formProps.state) {
    errors.state = "Required";
  }
  if (!formProps.country) {
    errors.country = "Required";
  }
  if (!formProps.zipcode) {
    errors.zipcode = "Required";
  } else if (/\D/.test(formProps.zipcode)) {
    errors.zipcode = "Zipcode allowed only number";
  }
  if (!formProps.firstName) {
    errors.firstName = "First name is Required";
  }else if(formProps.firstName.includes('.')){
     errors.firstName = "Please enter valid first name";
  }

  if (!formProps.lastName) {
    errors.lastName = "Last name is required";
  }else if(formProps.lastName.includes('.')){
    errors.lastName = "Please enter valid last name";
  }

  if (!formProps.firstNameCar) {
    errors.firstNameCar = "First name is Required";
  }else if(formProps.firstNameCar.includes('.')){
    errors.firstNameCar = "Please enter valid first name";
 }

  if (!formProps.lastNameCar) {
    errors.lastNameCar = "Last name is required";
  }else if(formProps.lastNameCar.includes('.')){
    errors.lastNameCar = "Please enter valid last name";
 }

  if (!formProps.dob) {
    errors.dob = "Required";
  }

  if (!formProps.email) {
    errors.email = "Required";
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formProps.email)
  ) {
    errors.email = "Invalid Email Address";
  }
  if (!formProps.code) {
    errors.code = "Required";
  }
  if (!formProps.phoneNumber) {
    errors.phoneNumber = "Required";
  }

  return errors;
};

const mapStateToProps = state => ({
  hotel: state.hotelReducer.hotel,
  sessionId: state.hotelReducer.sessionId,
  searchDate: state.hotelReducer.searchDate,
  pricedTotalFare: state.hotelReducer.pricedTotalFare,
  quotedTotalFare: state.hotelReducer.quotedTotalFare,
  fareBreakup: state.hotelReducer.fareBreakup,
  pricedRooms: state.hotelReducer.pricedRooms,
  rates: state.hotelReducer.rates,
  requestedOccupancies: state.hotelReducer.requestedOccupancies,
  roomPriceInfo: state.hotelReducer.roomPriceInfo,
  roomOccupancies: state.hotelReducer.roomOccupancies,
  rooms: state.hotelReducer.rooms,
  loginDetails: state.loginReducer.loginDetails,

  // loginDetails: state.loginReducer.loginDetails,
  paymentDetails: state.paymentReducer.paymentDetails,
  getCardDetails: state.cardReducer.getCardDetails,
  bookingConfirm: state.paymentReducer.bookingConfirm,
  loginStatus: state.loginReducer.loginStatus,
  bookingConfirmFail: state.paymentReducer.bookingConfirmFail,
  paymentFailureDetails: state.paymentReducer.paymentFailureDetails,
  bookingFailed: state.paymentReducer.bookingFailed,
  selectedCurrency: state.commonReducer.selectedCurrency
});
const mapDispatchToProps = dispatch => ({
  bookingReset: () => dispatch(bookingReset()),
  payment: payload => dispatch(payment(payload)),
  getCard: email => dispatch(getCard(email)),
  loadingGifSearch : () => dispatch(loadingGifSearch()),
  stopGifSearching : () => dispatch(stopGifSearching()),
  dumpValue: (data) => dispatch(dumpValue(data)),
  replaceItinerary:()=> dispatch(replaceItinerary([])),
  loginInfo: (guestFlag, loginpayload ,payloadInfo) => dispatch(loginInfo(guestFlag, loginpayload ,payloadInfo)),
});



class MultipleBooking extends Component {

  render(){
    return( 
      <MultipleContext.Consumer>
      { ({allBookingPayloadCar,allBookingPayloadHotel,addUpdatedPaymentResult}) =>{ 
        return( 
         <MulipleBookingPaymentView context={{allBookingPayloadCar,allBookingPayloadHotel,addUpdatedPaymentResult}} {...this.props}/>
        )
      }}
      </MultipleContext.Consumer>
  )
  }
 
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(
    reduxForm({
      form: "paymentMulti",
      validate: fieldValidation
    })(MultipleBooking)
  )
);

MulipleBookingPaymentView.propTypes = {
  isdivHide: propTypes.bool
};
