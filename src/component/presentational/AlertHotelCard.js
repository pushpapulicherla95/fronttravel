import React from 'react';

const AlertHotelCard = (props) => {
  const { alertInfo } = props;
  if(props.type=="hotel")
  {
  return(
  
    <div className="sectionCard marginTop0">
      <span className="noHotels"><i className="fas fa-hotel"></i></span>
      <h2 className="noHotelText">{alertInfo}</h2>
    </div>
    
  )
}
else if(props.type=="car")
{
return(

  <div className="sectionCard marginTop0">
    <span className="noHotels"><i className="fas fa-car"></i></span>
    <h2 className="noHotelText">{alertInfo}</h2>
  </div>
  
)
}
}

export default AlertHotelCard;