import React, { Component } from "react";
import { withRouter, NavLink } from "react-router-dom";
import Footer from '../component/Footer';
import TopNav from '../component/container/TopNav';
import  ScrollToTop from '../view/ScrollToTop';
import Notifications, { notify } from "react-notify-toast";
import URL from '../asset/configUrl'
import axios from '../service/Axios';
import SignIn from "../component/container/login/SignInModal";
import Subscription from '../component/Subscription';

class TermsAndCondition extends Component{
    constructor(props) {
        super(props)
        {
            this.state = {
              firstName: "",
              lastName: "",
              email: "",
              mobile: "",
              details: "",
              formErrors: { firstName: "", lastName: "", mobile:"" ,email:""},
              emailValid: false,
              firstNameValid: false,
              lastNameValid: false,
              mobileValid: false,
              emailValid:false,
              formValid: false,
              isdivHide: false,
              isgoPro:false
            };
        }
        
    }

    componentDidMount() {
        window.scrollTo(0, 0)
      }

      handleChange=(e)=>{
          const name= e.target.name;
          const value=e.target.value;
          this.setState({[name]:value},
             () => { this.validateField(name, value) 
          })
      }
      
validateField(fieldName, value) {
  let fieldValidationErrors = this.state.formErrors;
  let emailValid = this.state.emailValid;
  let firstNameValid = this.state.firstNameValid;
  let lastNameValid = this.state.lastNameValid;
  let mobileValid = this.state.mobileValid;

  switch (fieldName) {
    case "email":
      emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      fieldValidationErrors.email = emailValid ? "" : "Email is invalid";
      break;
    case "firstName":
      firstNameValid = value.length >= 3;
      fieldValidationErrors.firstName = firstNameValid ? "" : "FirstName is too short";
      break;
    case "lastName":
      lastNameValid = value.length == 0;
      fieldValidationErrors.lastName = lastNameValid ? "Required" : "";
      break;
    case "mobile":
      mobileValid = value.length == 0;
      fieldValidationErrors.mobile = mobileValid ? "Required" : "";
      break;
    default:
      break;
  }
  this.setState({formErrors: fieldValidationErrors,
                  emailValid: emailValid,
                  firstNameValid: firstNameValid,
                  lastNameValid:lastNameValid,
                  mobileValid:mobileValid
                }, this.validateForm);
}

validateForm() {
  this.setState({formValid: this.state.emailValid && this.state.firstNameValid && this.state.lastNameValid && this.state.mobileValid});
}

handleGopro = value => {
    this.setState({ isgoPro: value });
  };

  handleSignIn = () => {
    this.setState({ isVisibleSignIn: true });
    this.setState({ isdivHide: true });
  };

  onClose = () => {
    this.setState({ isVisibleSignIn: false });
  };
handleSubmit=(e)=>{
   e.preventDefault()
    const payload={ 
        "first_name":this.state.firstName,
        "email":this.state.email,
        "last_name":this.state.lastName,
        "phone_number":this.state.mobile,
        "message_details":this.state.details
    }
   axios.post(URL.CONTACT_DETAILS,payload)
   .then(response=>{
         notify.show( response.data.data, "success", 3000);
   })
   .catch(error=>{
    notify.show( error.response.data.data, "error", 3000);
   })
}


    render(){
        const {formErrors,isVisibleSignIn,isdivHide}=this.state;
        const renderSignInModal = isVisibleSignIn && (
            <SignIn onHide={this.onClose} isdivHide={isdivHide} />
          );
      
          const subscription = this.state.isgoPro && (
            <Subscription handleGopro={this.handleGopro} />
          );
        return(
            <React.Fragment>
       <Notifications/>
            <TopNav          
                 onSignIn={this.handleSignIn}
                {...this.props}
                handleGopro={this.handleGopro} />
                {renderSignInModal}
        {subscription}
     
            <section class="contactUsBg">
                <div className="container">
                    <div className="contactUsTitle"><h3>Terms and Conditions</h3></div>


                        <p>Last updated: April 15, 2019</p>


                        <p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the https://xeniapp.com/ website (the "Service") operated by Xeniapp ("us", "we", or "our").</p>

                        <p>Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.</p>

                        <p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</p>




                        <h2>Purchases</h2>

                        <p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information.</p>

                        <p>You represent and warrant that: (i) you have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete.</p>

                        <p>The service may employ the use of third party services for the purpose of facilitating payment and the completion of Purchases. By submitting your information, you grant us the right to provide the information to these third parties subject to our Privacy Policy.</p>

                        <p>We reserve the right to refuse or cancel your order at any time for reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons.</p>

                        <p>We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal transaction is suspected.</p>


                        <h2>Availability, Errors and Inaccuracies</h2>

                        <p>We are constantly updating product and service offerings on the Service. We may experience delays in updating information on the Service and in our advertising on other web sites. The information found on the Service may contain errors or inaccuracies and may not be complete or current. Products or services may be mispriced, described inaccurately, or unavailable on the Service and we cannot guarantee the accuracy or completeness of any information found on the Service.</p>

                        <p>We therefore reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.</p>








                        <h2>Accounts</h2>

                        <p>When you create an account with us, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.</p>

                        <p>You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>

                        <p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.</p>
                        

                        <p>We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in our sole discretion.</p>




                        <h2>Intellectual Property</h2>

                        <p>The Service and its original content, features and functionality are and will remain the exclusive property of Xeniapp and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Xeniapp.</p>


                        <h2>Links To Other Web Sites</h2>

                        <p>Our Service may contain links to third party web sites or services that are not owned or controlled by Xeniapp</p>

                        <p>Xeniapp has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.</p>

                        <p>You acknowledge and agree that Xeniapp shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.</p>

                        <p>We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.</p>


                        <h2>Termination</h2>

                        <p>We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.</p>

                        <p>If you wish to terminate your account, you may simply discontinue using the Service.</p>

                        <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>


                        <h2>Indemnification</h2>

                        <p>You agree to defend, indemnify and hold harmless Xeniapp and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password, or b) a breach of these Terms.</p>
                            
                            

                        <h2>Limitation Of Liability</h2>

                        <p>In no event shall Xeniapp, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>


                        <h2>Disclaimer</h2>

                        <p>Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.</p>

                        <p>Xeniapp its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.</p>


                        <h2>Exclusions</h2>

                        <p>Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.</p>



                        <h2>Governing Law</h2>

                        <p>These Terms shall be governed and construed in accordance with the laws of New York, United States, without regard to its conflict of law provisions.</p>

                        <p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.</p>


                        <h2>Changes</h2>

                        <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>

                        <p>By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.</p>


                        <h2>Contact Us</h2>

                        <p>If you have any questions about these Terms, please contact us.</p>
                        <ul>
                            <li>By email: info@xeniapp.com</li>
          
                        </ul>
                    
                    <ScrollToTop/>
                </div>
            </section>
            <Footer/>
            </React.Fragment>
        )
    }

}
export default TermsAndCondition;

