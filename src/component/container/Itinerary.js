import React from 'react';
import { connect } from 'react-redux';
import { addItinerary, removeItinerary ,saveWishList } from '../../service/addCart/action';
import { map as _map,
  sumBy as _sumBy} from 'lodash';
import img_DragDrop from  '../../asset/images/Drag-and-Drop-Section.png'
import img_ItineraryTourist from  "../../asset/images/Your Itinerary Tourist.svg";
import img_hotel from "../../asset/images/hotel-building.png";
import img_plane from "../../asset/images/plane.png";
import img_car from "../../asset/images/car.png";
import img_DateArrow from "../../asset/images/Date Arrow.png"; 
import img_ItineraryExpanded from "../../asset/images/Itinerary-Expanded-Lines.png";
import itinerary from "../../asset/images/itinerary.png"
import { DropTarget, ConnectDropTarget, DropTargetMonitor } from 'react-dnd'
import { withRouter } from 'react-router-dom';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import img_calender from '../../asset/images/dashboard/calender.png';
import img_Time from '../../asset/images/Time.svg';
import moment from 'moment';

var currencies = require('country-data').currencies;

class Itinerary extends React.Component {
  state = {
    isExpand: false,
    allCartDetails:[],
    wishListSaveAlert:false,
    isModal:false,
    isSameSetOfPassangers:false,
    currentDragItem:null,
    passangerError:false,
    passangerErrorMessage:"",
    randomRender:Math.random(),
    collpseIti:{}
  }
  
  componentDidUpdate(prevProps) {
    if (!prevProps.isOver && this.props.isOver) {
      // You can use this as enter handler
    }

    if (prevProps.isOver && !this.props.isOver) {
      // You can use this as leave handler
    }

    if (prevProps.isOverCurrent && !this.props.isOverCurrent) {
      // You can be more specific and track enter/leave
      // shallowly, not including nested targets
    }
  }

  handleRemoveItinerary = (item) => {
    this.props.removeFromItinerary(item);
  }

  componentWillReceiveProps(nextProps) {
     sessionStorage.setItem("itinerary",JSON.stringify(nextProps.itineraryList))
      //  console.log("next props",nextProps)
       if((this.props.wishlistStatus != nextProps.wishlistStatus)  && nextProps.wishlistStatus && nextProps.wishlistStatus.hasOwnProperty("status") && nextProps.wishlistStatus.status == "success"){
   
          this.setState({
            wishListSaveAlert:true
          })
          this.props.history.push("/dashboard/wish-list")
          setTimeout(()=>{

            this.setState({
              wishListSaveAlert:false
            })
          },3000)
       }
   
   }

  onSaveHandle=()=>{

    if(this.props.itineraryList.length != 0){
      //this.props.history.push("/hotel/multiplebooking")
      if(this.props.loginDetails){
        const { email } = this.props.loginDetails;
        this.props.saveWishList({data:this.props.itineraryList,email})
     }else{
      console.log("Logged out")
      this.setState({
        passangerError:true,
        passangerErrorMessage:"Log in to add wishlist"
      })
      setInterval(()=>{
        this.setState({
          passangerError:false,
          passangerErrorMessage:""
        })
      },5000)
     }
    }else{
      this.setState({
        passangerError:true,
        passangerErrorMessage:"No items added to itinerary"
      })

      setTimeout(()=>{
        this.setState({
          passangerError:false,
          passangerErrorMessage:""
        })
      },4000)
    }
  
  }
  
  diffPassangersCheck=(dragItem)=>{
    if (this.props.itineraryList.find(wishListData => {
       if(wishListData.type.includes("hotel")){
        return wishListData.type == dragItem.type && wishListData.bookingData.hotel.id != dragItem.bookingData.hotel.id 
       }else{
        return wishListData.type == dragItem.type 
       }
      })) {
      this.setState({
        passangerError:true,
        passangerErrorMessage:"Only one hotel and one car allowed"
      })
      setInterval(()=>{
        this.setState({
          passangerError:false,
          passangerErrorMessage:""
        })
      },5000)
    }else{
      this.props.addToItinerary(dragItem)
    }
  }

  onPassangerTypeChange(setValue){
    this.setState({
      isSameSetOfPassangers:setValue,
      isModal:false
    },()=>{

     if(setValue == true){
      if(this.props.itineraryList.length != 0){
        let currency= this.props.itineraryList[0].bookingData.currency
         if(currency.includes(this.state.currentDragItem.bookingData.currency)){
          this.props.addToItinerary(this.state.currentDragItem)
         }
         else{
             this.setState({
               passangerError:true,
               passangerErrorMessage:"Different currency cannot be added in itinerary"
             })
             setTimeout(()=>{
               this.setState({
                 passangerError:false,
                 passangerErrorMessage:""
               })
             },6000)
         }
        
       }else{
        this.props.addToItinerary(this.state.currentDragItem)
       }
    
     } else {

      if(this.props.itineraryList.length != 0){
        let currency= this.props.itineraryList[0].bookingData.currency
         if(currency.includes(this.state.currentDragItem.bookingData.currency)){
          this.diffPassangersCheck(this.state.currentDragItem)
         }
         else{
             this.setState({
               passangerError:true,
               passangerErrorMessage:"Different currency cannot be added in itinerary"
             })
             setTimeout(()=>{
               this.setState({
                 passangerError:false,
                 passangerErrorMessage:""
               })
             },4000)
         }
        
       }else{
        this.diffPassangersCheck(this.state.currentDragItem)
       }
      
     }
     this.setState({
       randomRender:Math.random()
     })
    })
  }
  // componentWillUnmount() {
   
  // }

  renderCar=(item)=>{
   
    return(
      <div>
         <h2><img src={img_car} /> <span> {item.title}</span></h2>
                               <ul>
                                 <li><img src={img_calender}/></li>
                                 <li>
                                   <div className="dateShow"><span>{moment(item.bookingData.carPickUpDate).format("DD MMM")}</span><span>{moment(item.bookingData.carPickUpDate).format("ddd")}</span></div> 
                                     <div className="timeShow"><img src={img_Time}/> {item.bookingData.carPickUpTime}</div>
                                  </li>
                                  <li><img src={img_DateArrow}/></li>
                                  <li>
                                   <div className="dateShow"><span>{moment(item.bookingData.carDropDate).format("DD MMM")}</span><span>{moment(item.bookingData.carDropDate).format("ddd")}</span></div> 
                                     <div className="timeShow"><img src={img_Time}/> {item.bookingData.carDropTime}</div>
                                  </li>
                               </ul>
                               <ul className="rateShowTable">
                                 <li><span>{moment(item.bookingData.carDropDate).diff(moment(item.bookingData.carPickUpDate), 'days')} days</span> <span>{(+item.bookingData.carRental.fare.displayFare.totalFare / moment(item.bookingData.carDropDate).diff(moment(item.bookingData.carPickUpDate), 'days') ).toFixed(2)} per day</span></li>
                                 {/* <li><span>Taxes & Fees</span> <span>$0</span></li> */}
                                 <li><span>Total</span> <span>{currencies[item.bookingData.currency].symbol} {item.price}</span></li>
                               </ul>
        </div>
    )

  }
  renderHotel=(item)=>{
    return(
      <div>
         <h2><img src={img_hotel} /> <span> {item.title}</span></h2>
                               <ul>
                                 <li><img src={img_calender}/></li>
                                 <li>
                                 <div className="dateShow"><span>{moment(item.searchString.checkin).format("DD MMM")}</span><span>{moment(item.searchString.checkin).format("ddd")}</span></div> 
                                   <div className="timeShow"><img src={img_Time}/> {item.bookingData.hotel.hasOwnProperty("checkinCheckoutPolicy") && item.bookingData.hotel.checkinCheckoutPolicy.length > 0  ?  item.bookingData.hotel.checkinCheckoutPolicy[0].inTime:"12:00"}</div>
                                </li>
                                <li><img src={img_DateArrow}/></li>
                                <li>
                                 <div className="dateShow"><span>{moment(item.searchString.checkout).format("DD MMM")}</span><span>{moment(item.searchString.checkin).format("ddd")}</span></div>
                                   <div className="timeShow"><img src={img_Time}/> {item.bookingData.hotel.hasOwnProperty("checkinCheckoutPolicy") && item.bookingData.hotel.checkinCheckoutPolicy.length > 0 ?  item.bookingData.hotel.checkinCheckoutPolicy[0].outTime:"12:00"}</div>
                                </li>
                               </ul>
                               <ul className="rateShowTable">
                                 {item.bookingData.room.map((value,index)=>{

                                   return    <li><span>{value.name} <strong>X</strong> {value.roomCount}</span> <span>{currencies[item.bookingData.currency].symbol} {(+value.baseFare / +item.bookingData.bookingDays).toFixed(2)}/Night</span></li>
                                 })}
                                 <li><span>Total</span> <span>{currencies[item.bookingData.currency].symbol} {item.price}</span></li>
                               </ul>
        </div>
    )
  }
 
  render() {
    const { connectDropTarget, isOver, canDrop , itineraryList } = this.props
    const { isExpand } = this.state;
    
    const selectedCurrencyVal = currencies[this.props.selectedCurrency].symbol;
    console.log("props",itineraryList)
    // const totalPrice = Math.round(_sumBy(itineraryList, 'price')) 
     //  const totalPrice=     _sumBy(itineraryList, function(o) { return +o.price })
    return connectDropTarget(
      <div>
   { this.state.randomRender && <div className= { isExpand ? "rightSide align-self-start leftItinerayShow" : "rightSide align-self-start"}>
      <div className="itineraryResponsiveBtn" onClick={() => this.setState({isExpand: !isExpand})}>
           YOUR ITINERARY
      </div>
        <h4 className="rightHeadText">
      
           YOUR ITINERARY
        </h4>
       
        <div className="itineraryBoxDesign">
        <img src={itinerary} alt="" /></div>
    
        <div className="itineraryItems">
          {this.state.wishListSaveAlert==true && <span  style={{color:"green"}} ><center><strong>&#10003; Added to wishlist</strong></center></span>}
          {this.state.passangerError==true && <span className="itineraryError" ><center><strong> {this.state.passangerErrorMessage} </strong></center></span>}
          <img src={img_DragDrop} className={!itineraryList.length 
            ? canDrop
              ? "dragDropImage is-ready" : "dragDropImage" 
            : "dragDropImage is-hidden"} alt="" />
          <ul className="itinerarySelItems">

            
          {_map(itineraryList, (item, index) => {

             let displayImg;
             let displayView;
             if(item.type=="car"){
               displayImg=img_car
               displayView=this.renderCar(item)
             }else if(item.type=="hotel") {
               displayImg=img_hotel
               displayView=this.renderHotel(item)
             }

             let collpseIti=this.state.collpseIti
             if(!collpseIti["itinerary"+index]){
               collpseIti["itinerary"+index]=false
             }

             return(
               
              <li key={index}>
                <div className="itineraryTitle">
                <div>
                  <a>
                    {" "}
                    <img src={displayImg} alt="" />
                  </a>
                  <h4 title={item.title}>
                    {/* {item.type.toUpperCase()} - */}
                    {item.title}
                    <span style={__titleStyle} title= {item.subtitle}>{item.subtitle} 
                    </span>
                    {/* <span style={__titleStyle} title= {item.subtitle}>
                      {item.subtitle} <img src={img_DateArrow} alt="" /> IAH
                    </span> */}
                  </h4>
                  <span>
                   {currencies[item.bookingData.currency].symbol} {item.price}
                  </span>
                </div>
                <a href="#" className="closeBtn" onClick={() => this.handleRemoveItinerary(item)}>
                      <i className="fas fa-times" />
                    </a>
                <div className="expandIcon" onClick={() => {
                  let collpseIti = this.state.collpseIti
                  collpseIti["itinerary" + index] = !collpseIti["itinerary" + index]
                  this.setState({ collpseIti })
                }}>
                  <img src={img_ItineraryExpanded} alt="" />
                </div>
                </div>
                <Collapse isOpen={this.state.collpseIti["itinerary"+index]} className="itineraryCollapsecontent">
                          <Card>
                            <CardBody className="">
                             {displayView}
                            </CardBody>
                          </Card>
                        </Collapse>   
              </li>
              )

          })}
          </ul>
        </div>
        <h2 className="itineraryTotal">Total : {itineraryList.length != 0 ? currencies[itineraryList[0].bookingData.currency].symbol : selectedCurrencyVal} {_sumBy(itineraryList,"price").toFixed(2)}</h2>
        <div className="itineraryBtns">
          <button type="button" onClick={this.onSaveHandle} className="searchBtn itinerarySave mr-1">
            Save <i className="far fa-thumbs-up" />
          </button>
          <button type="button" className="searchBtn itineraryBook" onClick={()=>{
            let currency
            if(itineraryList && itineraryList.length != 0){
              currency=itineraryList[0].bookingData.currency
            }else{
              currency=this.props.selectedCurrency
            }
            if(itineraryList.length != 0){
              this.props.history.push("/hotel/multiplebooking")
            }else{
              this.setState({
                passangerError:true,
                passangerErrorMessage:"No items added to itinerary"
              })

              setTimeout(()=>{
                this.setState({
                  passangerError:false,
                  passangerErrorMessage:""
                })
              },4000)
            }
            
          }}>
            Book <i className="fas fa-check" />
          </button>
        </div>
      </div>}

         {this.state.isModal === true && (
          <div
            className="modal backgroundDark"
            id="myModal"
            style={{ display: "block" }}
          >
            <div className="modal-dialog signInPopup" style={{ top:'15%'}} >
              <div className="modal-content" style={{ borderRadius:'6px'}}>
                <div className="modal-body paymentError">
                  <div className="socialBtnGroup" />
                  <h5 style={{fontWeight:'300',color:'#464646',padding:'20px 0px'}}>
                   <i class="fas fa-info-circle mr-1"></i>
                    Are you Booking for same set of passengers?
                  </h5>
                  <div className="mt-3">
                    <button
                    type="button"
                    className="goBack mr-2 bg-success"
                    onClick={this.onPassangerTypeChange.bind(this,true)}
                  >
                    Yes
                  </button>
                  <button
                    type="button"
                    className="goBack bg-danger"
                    onClick={this.onPassangerTypeChange.bind(this,false)}
                  >
                    No,Different Passengers
                  </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
    </div>
    
    );
  }
}

const __handleDrop = {
  canDrop: (props, monitor)=> {
    const item = monitor.getItem();
    // component.setState({
    //   currentDragItem: monitor.getItem()
    // })
    return true;
  },

  hover:(props, monitor, component)=> {
 
  },

  drop:(props, monitor, component)=> {
    if (monitor.didDrop()) {
      // If you want, you can check whether some nested
      // target already handled drop
      return;
    }
    const item = monitor.getItem();
   console.log('dropp...', item, props);

  component.setState({
    currentDragItem: monitor.getItem()
  },async ()=>{



    if(props.itineraryList.length == 0){
        component.setState({collpseIti:{}})
        props.addToItinerary(item);
     }else if(props.itineraryList.length == 1 ){
      let dataFindHotel
      if(item.type=='hotel'){
        dataFindHotel=props.itineraryList.find((data)=>{
           if(data.type=="hotel"){
             return data.bookingData.hotel.id==item.bookingData.hotel.id
           }
        })
      }
     
      if(!dataFindHotel){
        await component.setState({
          isModal:true
       })
      }
  
     }
    if(component.state.isSameSetOfPassangers == true && component.state.isModal == false){
      if(props.itineraryList.length != 0){
       let currency= props.itineraryList[0].bookingData.currency
        if(currency.includes(item.bookingData.currency)){
          props.addToItinerary(item);
        }
        else{
            component.setState({
              passangerError:true,
              passangerErrorMessage:"Different currency cannot be added in itinerary"
            })
            setTimeout(()=>{
              component.setState({
                passangerError:false,
                passangerErrorMessage:""
              })
            },4000)
        }
       
      }else{
        props.addToItinerary(item);
      }
   
    }else if( component.state.isSameSetOfPassangers == false && component.state.isModal == false){

      if(props.itineraryList.length != 0){
        let currency= props.itineraryList[0].bookingData.currency
         if(currency.includes(item.bookingData.currency)){
          component.diffPassangersCheck(item)
         }
         else{
             component.setState({
               passangerError:true,
               passangerErrorMessage:"Different currency cannot be added in itinerary"
             })
             setTimeout(()=>{
               component.setState({
                 passangerError:false,
                 passangerErrorMessage:""
               })
             },4000)
         }
        
       }else{
        component.diffPassangersCheck(item)
       }
    }
  })
    return { moved: true };
  }
};
const __collect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType()
  }
}

const mapStateToProps = state => ({
  itineraryList: state.addcartReducer.itineraryList,
  loginDetails: state.loginReducer.loginDetails,
  wishlistStatus: state.addcartReducer.wishlistStatus,
  selectedCurrency: state.commonReducer.selectedCurrency,
})

const mapDispatchToProps = dispatch => ({
   addToItinerary: (itPay) => dispatch(addItinerary(itPay)),
   removeFromItinerary: (itPay) => dispatch(removeItinerary(itPay)),
   saveWishList: (payload) => dispatch(saveWishList(payload))
})

export default  withRouter(connect(mapStateToProps, mapDispatchToProps)
  (DropTarget('ROOM', __handleDrop, __collect)(Itinerary)));

const __titleStyle = {
  width: '71px',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis'
};