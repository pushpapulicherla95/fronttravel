import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import InputRange from 'react-input-range';
import downArrow from '../../../asset/images/selarrow.png';

import { carFilter, updatePagingDetails } from '../../../service/car/action';

var currencies = require('country-data').currencies;

const _carTypeValuesArray = [
  "CargoVan",
  "Compact",
  "CompactElite",
  "Convertible",
  "Economy",
  "EconomyElite",
  "Estate",
  "Exotic",
  "ExoticSuv",
  "FifteenPassengerVan",
  "FiftyPassengerCoach",
  "FiveSeatMiniVan",
  "FourWheelDrive",
  "Fullsize",
  "FullsizeElite",
  "Intermediate",
  "IntermediateElite",
  "LargeSuv",
  "LargeTruck",
  "Luxury",
  "LuxuryElite",
  "MediumSuv",
  "MidSize",
  "Mini",
  "MiniElite",
  "MiniVan",
  "Moped",
  "MovingVan",
  "NineSeatMiniVan",
  "Oversize",
  "Premium",
  "PremiumElite",
  "Regular",
  "SevenSeatMiniVan",
  "SmallOrMediumTruck",
  "SmallSuv",
  "Special",
  "Standard",
  "StandardElite",
  "Stretch",
  "Subcompact",
  "SUV",
  "TwelveFootTruckSUV",
  "TwelvePassengerVan",
  "TwentyFootTruck",
  "TwentyFourFootTruck",
  "TwentySixFootTruck",
  "Unique",
]

class CarResultFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {
        min: 0,
        max: 10000
      },
      isFliterExpand: false,
      isCarTypeExpanded: true,
      selectedCarTypeArray: [],
      selectedVendorArray: [],
      checkbox: {},
      checkboxVendor: {},
      checkboxAuto: false,
      checkboxManual: false,
      checkboxLowToHigh:true,
      checkboxHighToLow:false,
    };
  }

  handleChange = value => {
    // let res = Object.assign({}, this.state.values); 
    //     res.min = value.min < 0 ? 0 : value.min;
    //     res.max = value.max > this.state.values.max ? this.state.values.max :value.max
    // this.setState({values: res });

 let newValue=value
     newValue.min=value.min < 0 ? 0 : value.min
     newValue.max=value.max > 10000 ? 10000 : value.max
   this.setState({values: newValue });
  };

  checkBoxHandler(index, contentType) {

    let selectedCarType = []
    let selectedVendor = []

    let checkbox = this.state.checkbox
    let checkboxVendor = this.state.checkboxVendor

    if (contentType == "type") {

      if (checkbox[index] == true) {
        checkbox[index] = false
      } else if (!checkbox[index]) {
        checkbox[index] = true
      }

    } else if (contentType == "vendor") {

      if (checkboxVendor[index] == true) {
        checkboxVendor[index] = false
      } else if (!checkboxVendor[index]) {
        checkboxVendor[index] = true
      }
    }

    this.setState({
      checkbox,
      checkboxVendor
    }, () => {
      Object.keys(checkbox).map((key, index) => {
        if (this.state.checkbox[key] == true) {
          selectedCarType.push(_carTypeValuesArray[key])
        }
      })

      Object.keys(checkboxVendor).map((key, index) => {
        if (this.state.checkboxVendor[key] == true) {
          selectedVendor.push(this.props.vendorList[key].code)
        }
      })

      this.setState({
        selectedCarTypeArray: selectedCarType,
        selectedVendorArray: selectedVendor
      }, () => {
        this.filterResultsCar()
      })
    })
  }

  filterResultsCar = () => {

    let transmissionType
    if (this.state.checkboxAuto && !this.state.checkboxManual) {
      transmissionType = "Automatic"
    } else if (this.state.checkboxManual && !this.state.checkboxAuto) {
      transmissionType = "Manual"
    } else {
      transmissionType = ""
    }

    let orderBy
    if(this.state.checkboxLowToHigh){
      orderBy="price asc"
    }else if(this.state.checkboxHighToLow){
      orderBy="price desc"
    }


    let email=null
    if(this.props.loginDetails){
      email=this.props.loginDetails.email
    }

    let filterPayload = {
      email,
      sessionId: this.props.sessionId,
      "currency": this.props.selectedCurrency,
      "paging": {
        "pageNo": 1,
        "pageSize": 30,
        "orderBy": orderBy
      },
      "contentPrefs": [
        "All"
      ],
      "filters": {
        "vehicleType": {
          "allow": this.state.selectedCarTypeArray
        },
        "vendor": {
          "allow": this.state.selectedVendorArray
        },
        "price": {
          "min": +this.state.values.min,
          "max": +this.state.values.max
        },
        "transmission": transmissionType
      }
    }
    // if (this.state.selectedCarTypeArray.length == 0 && this.state.selectedVendorArray.length == 0) {
    //   this.props.updatePagingDetails(null)

    //   this.props.filterCar({
    //     email,
    //     sessionId: this.props.sessionId,
    //    "currency": this.props.selectedCurrency,
    //    "paging": {
    //       "pageNo": 1,
    //       "pageSize": 30,
    //       "orderBy": "price asc"
    //     },
    //     "filters": {
    //       "price": {
    //         "min": +this.state.values.min,
    //         "max": +this.state.values.max
    //       },
    //       "transmission": transmissionType
    //     }
    //   })
    // } else {
      this.props.updatePagingDetails(filterPayload)
      this.props.filterCar(filterPayload)
    // }


  }

  render() {
    const { isFliterExpand } = this.state;
    const { selectedCurrency } = this.props;
    const selectedCurrencyVal = currencies[this.props.selectedCurrency].symbol;
    return (
      <div id="carFilterBg" className={
        isFliterExpand
          ? "filterBg flex-column align-self-start showFilterBg"
          : "filterBg flex-column align-self-start"
      }
      >
        <h2>FILTER BY</h2>
        <div className="filterTitle" onClick={() => this.setState({ isFliterExpand: !isFliterExpand })} >
          <h2> FILTER BY  <img src={downArrow} className="downArrowImg" /> </h2></div>
        <div className="respDeskShow">
          <div>
            <h4>Price Range</h4>
            <div className="slideRange">
                <InputRange
                  maxValue={10000}
                  minValue={0}
                  formatLabel={value => ``}
                  onChangeComplete={value => this.filterResultsCar()}
                  onChange={value => this.handleChange(value)}
                  value={this.state.values}
                />
            </div>
            <span className="rangeVauleLeft"><span style={{ fontSize: '16px' }}>{selectedCurrencyVal}</span>&nbsp;{this.state.values.min}</span>
            <span className="rangeVauleRight"><span style={{ fontSize: '16px' }}>{selectedCurrencyVal}</span>&nbsp;{this.state.values.max}</span>
          </div>
          <div>
          <h4>Sort Price</h4>
          <ul className="priceRange">
            <li><input
                    className="filtercheckbox"
                    id="priceRange90"
                    type="checkbox"
                    onChange={()=>{this.setState({checkboxLowToHigh:!this.state.checkboxLowToHigh,checkboxHighToLow:false},()=>this.filterResultsCar())}}
                    checked={this.state.checkboxLowToHigh}
                  />
                  <label htmlFor="priceRange90">
                    <p>Low to High </p>
                  </label></li>
                  <li><input
                    className="filtercheckbox"
                    id="priceRange91"
                    type="checkbox"
                    onChange={()=>{this.setState({checkboxHighToLow:!this.state.checkboxHighToLow,checkboxLowToHigh:false},()=>this.filterResultsCar())}}
                    checked={this.state.checkboxHighToLow}
                  />
                  <label htmlFor="priceRange91">
                    <p>High to Low </p>
                  </label></li>
            </ul>
          </div>
         
          <div>
            <h4>Car Transmission</h4>
            <ul>
              <li >
                <input
                  id="acc_877"
                  type="checkbox"
                  className="filtercheckbox"
                  value={this.state.checkboxAuto}
                  onChange={() => { this.setState({ checkboxAuto: !this.state.checkboxAuto }, () => { this.filterResultsCar() }) }}
                />
                <label htmlFor={`acc_877`} >Automatic</label>
              </li>
              <li >
                <input
                  id="acc_787"
                  type="checkbox"
                  className="filtercheckbox"
                  value={this.state.checkboxManual}
                  onChange={() => { this.setState({ checkboxManual: !this.state.checkboxManual }, () => { this.filterResultsCar() }) }}

                />
                <label htmlFor={`acc_787`}>Manual</label>
              </li>
            </ul>
          </div>
          <div>
            <h4>Car Type</h4>         <ul>
              {_carTypeValuesArray.map((each, i) => {

                if (this.state.isCarTypeExpanded == false) {
                  return (
                    <li key={i}>
                      <input
                        type="checkbox"
                        id={`acc_${i}`}
                        name={each}
                        className="filtercheckbox"
                        value={this.state.checkbox[i]}
                        onChange={this.checkBoxHandler.bind(this, i, "type")}
                      />
                      <label htmlFor={`acc_${i}`}>{each}</label>
                    </li>
                  )
                }
                else if (this.state.isCarTypeExpanded == true) {
                  if (i <= 5) {
                    return (
                      <li key={i}>
                        <input
                          type="checkbox"
                          id={`acc_${i}`}
                          name={each}
                          className="filtercheckbox"
                          value={this.state.checkbox[i]}
                          onChange={this.checkBoxHandler.bind(this, i, "type")}
                        />
                        <label htmlFor={`acc_${i}`}>{each}</label>
                      </li>
                    )
                  }
                }
              })}
            </ul>

            <a className="showmore" style={{ color: '#00a0ff', fontSize: '14px', cursor: 'pointer' }}
              onClick={() => this.setState({ isCarTypeExpanded: !this.state.isCarTypeExpanded })}>
              {this.state.isCarTypeExpanded == true ? 'Show More' : "Show less"}
            </a>
          </div>
          <div>
            <h4>Car Company</h4>         <ul>
              {this.props.vendorList && this.props.vendorList.map((each, i) => (
                <li key={i}>
                  <input
                    type="checkbox"
                    id={`acc_1${i}`}
                    name={each}
                    className="filtercheckbox"
                    value={this.state.checkboxVendor[i]}
                    onChange={this.checkBoxHandler.bind(this, i, "vendor")}
                  />
                  <label htmlFor={`acc_1${i}`}>{each.name}</label>
                </li>
              ))}
            </ul>
          </div>
        </div>

      </div>
    )
  }
}

const mapStateToProps = state => ({
  sessionId: state.carReducer.sessionId,
  selectedCurrency: state.commonReducer.selectedCurrency,
  vendorList: state.carReducer.carVendorList,
  loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
  filterCar: filterInfo => dispatch(carFilter(filterInfo)),
  updatePagingDetails: payload => dispatch(updatePagingDetails(payload)),
})
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CarResultFilter));