import React, { Component } from 'react';
import CarCard from './CarCard';
import CarResultFilter from './CarResultFilter';

import queryString from 'query-string';
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";

import { carSearch } from '../../../service/car/action';
//import img_down from "../../../asset/images/downarrow.png";
import img_down from "../../../asset/images/downarrow.png";
import AlertHotelCard from "../../presentational/AlertHotelCard";
import { carFilter, carFilterLoadMore } from '../../../service/car/action';

class CarSearchResult extends Component {

    state = {
        pageNo: 1,
        offsetHeight:1000
    }

    componentDidMount() {
        this.getCarList(this.props.location)
    }
    

    componentWillReceiveProps(newProps) {
      
        if (this.props.location !== newProps.location) {
            this.getCarList(newProps.location);
        }
        if (newProps.hasOwnProperty("pagingDetails") && newProps.pagingDetails) {
            if (newProps.pagingDetails.paging.pageNo == 1) {
                this.setState({
                    pageNo: 1
                })
            }
        }
    }

    loadMore = () => {
        if (this.props.pagingDetails) {
            this.setState({
                pageNo: +this.state.pageNo + 1,
            }, () => {
                let oldPageDetails = this.props.pagingDetails
                oldPageDetails.paging.pageNo = +this.state.pageNo
                this.props.carFilterLoadMore(oldPageDetails)
            })
        } else {
            this.setState({
                pageNo: +this.state.pageNo + 1,
            }, () => {

                let email=null
                if(this.props.loginDetails){
                  email=this.props.loginDetails.email
                }
        
                let pay = {
                     email,
                    "sessionId": this.props.sessionId,
                    "currency": this.props.selectedCurrency,
                    "paging": {
                        "pageNo": this.state.pageNo,
                        "pageSize": 30,
                        "orderBy": "price asc"
                    },
                    "contentPrefs": [
                        "All"
                    ],
                    // "filters": {
                    //     "price": {
                    //         "min": 0,
                    //         "max": 10000
                    //     }
                    // }
                }
                this.props.carFilterLoadMore(pay)
            })
        }

    }

    getCarList = location => {
        const values = queryString.parse(location.search);

        let email=null
        if(this.props.loginDetails){
          email=this.props.loginDetails.email
        }

        let carSearchPayload = {
            email,
            currency: this.props.selectedCurrency,
            driverInfo: {
                age: values.driverAge,
                nationality: values.nationality
            },
            pickUp: {
                pickUpSearchString: values.pickUpLocation,
                date: values.carPickUpDate,
                time: values.carPickUpTime

            },
            dropOff: {
                dropOffSearchString: values.dropOffLocation,
                date: values.carDropDate,
                time: values.carDropTime
            },
            radiusKms: 10
        }
        this.props.searchCar(carSearchPayload);
    };

    render() {

        const { carList } = this.props;
        
        let clientWidth
        let offsetHeight
    
        try{
           clientWidth = document.body.clientWidth;
           if(+clientWidth > 1199){
            offsetHeight = document.getElementById("carFilterBg").offsetHeight;
           }else{
            offsetHeight=1000
           }
        }catch(e){
          offsetHeight=1000
        }

        
        return (

            <div className="d-flex flex-row tab-column justify-content-start">

                <CarResultFilter />

                <div className="filterResult">
                    <div className='cardDetailsHowBg' id="content" style={{height:offsetHeight+'px'}}>

                        {carList && carList.length != 0 && carList.map((singleCarData, index) => {

                            let vendorLogoDetails = this.props.carVendorList.find((singleVendor) => {
                                return singleVendor.code == this.props.carRentals[index].vendorCode
                            })
                            let Logo;
                            if (vendorLogoDetails) {
                                Logo = vendorLogoDetails.logo
                            } else {
                                Logo = ""
                            }
                            return (
                                <CarCard
                                    searchDetails={queryString.parse(this.props.location.search)}
                                    rentalId={this.props.carRentals[index].id}
                                    carDetails={singleCarData}
                                    carRentals={this.props.carRentals[index]}
                                    logoVendor={Logo}

                                />
                            )


                        })
                        }
                        {carList && carList.length == 0 &&
                            <AlertHotelCard type="car" alertInfo="No Cars Available" />
                        }


                        <div className="text-center">
                            {this.props.isLoadMoreAvailable && this.props.isLoadMoreAvailable == true &&
                                <button type='button' className='clickMoreBtn searchBtn' onClick={this.loadMore}><img src={img_down} alt='down' /></button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    carList: state.carReducer.carList,
    sessionId: state.carReducer.sessionId,
    carRentals: state.carReducer.carRentals,
    carVendorList: state.carReducer.carVendorList,
    pagingDetails: state.carReducer.pagingPayload,
    isLoadMoreAvailable: state.carReducer.isLoadMoreAvailable,
    selectedCurrency: state.commonReducer.selectedCurrency,
    countryCode: state.commonReducer.countryCode,
    loginDetails: state.loginReducer.loginDetails
});

const mapDispatchToProps = dispatch => ({
    searchCar: searchInfo => dispatch(carSearch(searchInfo)),
    carFilterLoadMore: filterInfo => dispatch(carFilterLoadMore(filterInfo))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CarSearchResult));