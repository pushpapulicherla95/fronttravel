import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import img_car from '../../../asset/images/car/carImg.png';
import img_logo from '../../../asset/images/car/carlogo.png';
import img_carUSer from '../../../asset/images/dashboard/carUser.png'
import img_carDoor from '../../../asset/images/dashboard/door.png'
import img_carLuggage from '../../../asset/images/dashboard/luggage.png';
import queryString from 'query-string';
import moment from 'moment';

var currencies = require('country-data').currencies;

class CarCard extends Component {

	carCardSelect = () => {
		this.props.history.push('/car/extra?' + queryString.stringify({
			...this.props.searchDetails,
			rentalId: this.props.rentalId,
			sessionId: this.props.sessionId,
			selectedCurrency:this.props.selectedCurrency
		}));
	}

	render() {

		const { carDropDate, carDropTime, carPickUpDate, carPickUpTime, driverAge, dropOffLocation, pickUpLocation } = queryString.parse(this.props.location.search)
		const { selectedCurrency } = this.props;
		const totalFare = this.props.carRentals.fare.displayFare.totalFare;
		const selectedCurrencyVal = currencies[this.props.selectedCurrency].symbol;

		return (
			<div className="sectionCard carCard">
				<div className="d-flex flex-row smallColumn">
					<div className="flex-column carImgDiv align-self-center">
						{this.props.carDetails.images && this.props.carDetails.images.length != 0 ?
							<img src={this.props.carDetails.images[0]} className="carImgWid" /> : <img src={""} alt="no Room Available" className="carLogoWidth" />}
						<img src={this.props.logoVendor} alt="No logo available" className="carLogoWidth" />
					</div>

					<div className="detailsBg flex-column carInfo">

						<h4>{this.props.carDetails.name}</h4>
						<ul className="carCategories">
							<li>{this.props.carDetails.type}</li>
							
							{this.props.carRentals.mileage.isUnlimited == true && <li>Unlimited Mileage</li>}
							<li>{this.props.carDetails.transmission}</li>
						</ul>

						<ul className="carInfraStru">
							<li>
								<img src={img_carUSer} /> {this.props.carDetails.passengerCapacity}
							</li>
							<li>
								<img src={img_carDoor} /> -
																	  </li>
							<li>
								<img src={img_carLuggage} /> {this.props.carDetails.baggageCapacity}
							</li>
						</ul>

						<ul className="pickupDropDet">
							<li>
								<h6>Pick-Up</h6>
								<p>{pickUpLocation}</p>
								<p>{moment(carPickUpTime, "hh:mm").format('LT')}</p>
							</li>
							<li>
								<h6>Drop-Off</h6>
								<p>{dropOffLocation}</p>
								<p>{moment(carDropTime, "hh:mm").format('LT')}</p>

							</li>
						</ul>

					</div>
					<div className="rateShowDiv flex-column">

						<div className="priceDiv">
							{/* <strike>{selectedCurrencyVal}680</strike> */}
							{<h2>{selectedCurrencyVal}&nbsp;{(+totalFare / moment(carDropDate).diff(moment(carPickUpDate), 'days')).toFixed(2)}</h2>}
							<p>per day</p>
						</div>
						<p className="totalAmt">Total :{selectedCurrencyVal}&nbsp;{totalFare}</p>
						<button type="button" className="selectRoomBtn" onClick={this.carCardSelect}>Select</button>
					</div>
				</div>
			</div>
		);
	}
}
const mapStateToProps = state => ({
	sessionId: state.carReducer.sessionId,
	selectedCurrency: state.carReducer.selectedCurrency
});

export default withRouter(connect(mapStateToProps)(CarCard));