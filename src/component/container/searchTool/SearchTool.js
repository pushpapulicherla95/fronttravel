import React from "react";
import { NavLink } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import queryString from 'query-string'
import DateRangePicker from "react-daterange-picker";
import originalMoment from "moment";
import { extendMoment } from "moment-range";
import "react-daterange-picker/dist/css/react-calendar.css";
import { map as _map } from "lodash";

import LocationSearch from "./LocationSearch";

import { searchHotel } from "../../../service/hotel/action";

import img_Calendar from "../../../asset/images/Calendar.svg";
import img_SearchIcon from "../../../asset/images/Search Icon.png";
import img_progress from "../../../asset/images/under-construction.png";
import img_flight from "../../../asset/images/airplane-shape.png";
import img_hotal from "../../../asset/images/hotel-building.png";
import img_car from "../../../asset/images/car.png";
import img_activities from "../../../asset/images/activities.png";
import img_clock from '../../../asset/images/car/wall-clock.png';
import img_age from '../../../asset/images/car/ageIcon.png';
import img_package from '../../../asset/images/Packages-Icon.png';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import img_takeOff from '../../../asset/images/flight/takeOff.png';
import img_Landing from '../../../asset/images/flight/Landing.png';
import img_plusCircle from '../../../asset/images/flight/plusCircle.png';
import img_minusCircle from '../../../asset/images/flight/minusCircle.png';
import img_country from '../../../asset/images/New folder/world.png';

import countryList from 'react-select-country-list'
import Select from 'react-select'

const moment = extendMoment(originalMoment);
const today = moment();

const style = {
  margin: "20px",
  display: "block",
  marginLeft: "auto",
  marginRight: "auto",
  width: "10%"
}

const staticBounds = {
  "bounds": {
    "rectangle": {
      "bottomRight": {
        "lat": 50.178005,
        "long": 3.921267
      },
      "topLeft": {
        "lat": 50.64359,
        "long": 2.635867
      }
    }
  },
}

const stateDefinitions = {
  available: {
    color: null,
    label: 'Available',
  },
  unavailable: {
    selectable: false,
    color: '',
    label: 'Unavailable',
  }
};
const dateRanges = [
  {
    state: 'unavailable',
    range: moment.range(today.clone().subtract(1, "days"), today.clone()),
  },
];
class SearchTool extends React.Component {
  
  state = {
    searchPayload: null,
    activeTab: "hotels",
    value: moment.range(today.clone().subtract(1, "days"), today.clone().add(1, 'days')),
    startDate: moment().format("MM/DD/YYYY"),
    endDate: moment().add(1, 'days').format("MM/DD/YYYY"),
    isCalendar: false,
    adult: "1",
    child: "0",
    room: '1',
    childAgeValues: [],
    childCounts: Array.from({ length: 12 }),
    roomCounts: Array.from({ length: 9 }),


    carPickUpDate:moment().add(1,'days').format("MM/DD/YYYY"),
    carDropDate:"",
    carPickUpTime:"00:00",
    carDropTime:"00:00",
    pickUpLocation:"",
    dropOffLocation:"",
    driverAge:"18",
    errorCarValidation:'',
    passangerCateg :false,
    wayOfTravel:'',
    countAdd:1,
    multiFlightAdd:[],
    addFlightField:false,

    options: countryList().getData(),
    nationality: '',
  };

  handleCarFormDataChange=(e)=>{
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value })
  }

  getSearchInfo = searchPayload => {
    this.setState({ searchPayload });
  };

  getSearchInfoPickUp = pickUpLocation => {
    this.setState({ pickUpLocation });
  };

   getSearchInfoDropOff = dropOffLocation => {
    this.setState({ dropOffLocation });
  };

  componentWillMount = () => {
    if (window.location.search) {
      const values = queryString.parse(window.location.search);
      if(window.location.search.includes("hotel"))
      {
      this.setState({
        startDate: values.checkin || values.startDate,
        endDate: values.checkout || values.endDate,
        adult: values.adult,
        child: values.child,
        childAgeValues: values.childAgeValues
      })
    }
    else if (window.location.search.includes("car")){
      this.setState({
        carPickUpDate:values.carPickUpDate,
        carDropDate:values.carDropDate,
        carPickUpTime:values.carPickUpTime,
        carDropTime:values.carDropTime,
        pickUpLocation:values.pickUpLocation,
        dropOffLocation:values.dropOffLocation,
        driverAge:values.driverAge,
      },()=>{
        this.props.history.push('/car/search'+window.location.search)
      })
    }
    }
  }

  componentDidMount(){

  }


  _tabs = [
    { value: "flights", lable: "FLIGHTS", to: '/flight', refimg:img_flight },
    { value: "hotels", lable: "HOTELS", to: '/hotel', refimg:img_hotal },
    { value: "cars", lable: "CARS", to: '/car', refimg:img_car },
    { value: "activities", lable: "ACTIVITIES", to: '/activity',refimg:img_activities },
    { value: "packages", lable: "EXPERIENCES", to: '/package' ,refimg:img_package}
  ];

  
  getToday = () => {
    return moment().format("MM/DD/YYYY");
  };

  changeHandlerNationality = nationality => {
    this.setState({ nationality })
  }

  handleSearch = (event) => {
    // event.preventDefault();

    this.setState({ isCalendar: false });

    const { searchPayload, startDate, endDate, adult, child, childAgeValues } = this.state;
   
    const searchInfo = {
      ...searchPayload,
      date: {
        start: startDate,
        end: endDate
      }
    };
    const searchString = {
      checkin: startDate,
      checkout: endDate,
      searchText: searchPayload.searchString,
      adult,
      child,
      childAgeValues
    }
    this.props.history.push('/hotel/search?' + queryString.stringify(searchString))
  };

  //TODO

  carHandleSearch = () =>{
 
  let { carPickUpDate,
    carDropDate,
    carPickUpTime,
    carDropTime,
    pickUpLocation,
    dropOffLocation,
    driverAge,
    nationality } = this.state;
    

 if( carPickUpDate &&  carDropDate &&  carPickUpTime && carDropTime &&  pickUpLocation  && driverAge &&  nationality){

  if(pickUpLocation && !dropOffLocation){
    this.props.history.push('/car/search?'+queryString.stringify({
      carPickUpDate:moment(carPickUpDate).format("YYYY-MM-DD"),
      carDropDate:moment(carDropDate).format("YYYY-MM-DD"),
      carPickUpTime,
      carDropTime,
      pickUpLocation:pickUpLocation.searchString,
      dropOffLocation:pickUpLocation.searchString,
      driverAge,
      nationality:this.state.nationality.value
    }))
  }else{
    this.props.history.push('/car/search?'+queryString.stringify({
      carPickUpDate:moment(carPickUpDate).format("YYYY-MM-DD"),
      carDropDate:moment(carDropDate).format("YYYY-MM-DD"),
      carPickUpTime,
      carDropTime,
      pickUpLocation:pickUpLocation.searchString,
      dropOffLocation:dropOffLocation.searchString,
      driverAge,
      nationality:this.state.nationality.value}))
  }

 }else{

  const validationIDs=["carPickUpDate",
  "carDropDate",
  "carPickUpTime",
  "carDropTime",
  "pickUpLocation",
  "driverAge",
  "nationality"]

  validationIDs.map((elememtId,index)=>{
    if(!this.state[elememtId]){
      document.getElementById(elememtId).setAttribute('STYLE','border-color:red')
    }else{
      document.getElementById(elememtId).removeAttribute('STYLE')
    }
  })
  
  setTimeout(()=>{
    validationIDs.map((elememtId,index)=>{
      try{
        document.getElementById(elememtId).removeAttribute('STYLE')
      }catch(e){

      }
    })
  },4000)

 }

}

handleFlightSearch = e => {
  this.props.history.push('/flight/search?' + queryString.stringify());
}

  handleStartDate = e => {
    this.setState({
      isCalendar: true,
      startDate: e.target.value,
      endDate: e.target.value
    });
  };

  handleEndDate = e => {
    this.setState({
      isCalendar: true,
      endDate: e.target.value
    });
   
  };

  onSelect = (value, states) => {
    this.setState({
      value,
      startDate: moment(value.start._d).format("MM/DD/YYYY"),
      isCalendar: false,
      endDate: moment(value.end._d).format("MM/DD/YYYY")
    });
  };

  handleSelectChange = (e) =>{
      this.setState({wayOfTravel : e.target.value});
     
  }

  addAnotherFlight = (e) => {
        if(this.state.multiFlightAdd.length < 6){
           this.setState({countAdd : this.state.countAdd+1})
           this.state.multiFlightAdd.push(this.state.countAdd);
           
        }else{
          this.setState({addFlightField:true});
        }

  }

  removeFlights = (e) => {
      const id = e.currentTarget.id;
      const items = this.state.multiFlightAdd.slice(0, id-1).concat(...this.state.multiFlightAdd.slice(id, this.state.multiFlightAdd.length))
      this.setState({multiFlightAdd:items,addFlightField:false});

  }

  render() {

    console.log("this.setach payload",this.state)
    return (
      <div className="mb-3">
        <ul className="tabs">
          {this._tabs.map((each, i) => (
            <li
              key={i}
              className={
                this.props.path === each.to ? "tab-link current" : "tab-link"
              }
              onClick={() => this.props.history.push(each.to)}
              data-tab="tab-1">
              <span className="responNameHide">{each.lable}</span>
              {each.refimg && <span className='responIconHide respIconSize'> <img src={each.refimg} width="20px" alt="" /></span>}
            </li>
          ))}
          <li className="tab-link">
            <i className="fas fa-chevron-right" />
          </li>
        </ul>

        <div id="tab-2" className="tab-content current">
          {this.renderContent()}
        </div>
      </div>
    );
  }
  renderContent = () => {

    const { startDate, endDate, isCalendar, adult, child, room, roomCounts,passangerCateg,multiFlightAdd,addFlightField } = this.state;

    switch (this.props.path) {
      case this._tabs[0].to:return(
        
                  <React.Fragment>
                      <h3 className="tabHeadText">Search for the best Flights</h3>
                      <div className="d-flex flex-row smallColumn flightSearch">
                        <div className="flex-column">
                          
                              <div className="seleTrip">
                                <select value={this.state.wayOfTravel} onChange={this.handleSelectChange}>
                                  <option value="one">One Way</option>
                                  <option value="round">Round Trip</option>
                                  <option value="multi">Multi Way</option>
                                </select>
                              </div>
                          
                         </div>
                         <div className="flex-column">
                            <div className="seleTrip">
                              <select>
                                <option>Economy</option>
                                <option>Premium Economy</option>
                                <option>Business</option>
                                <option>First</option>
                            </select>
                          </div>
                         </div>
                         <div className="flex-column">
                           <div className="selePassanger">
                                <span className="NoOfText"  onClick={() =>
                                this.setState({
                                  passangerCateg : !passangerCateg
                                })
                              }>Passanger</span>
                                <div className={ passangerCateg ? "typeOfPassanger" : "typeOfPassanger d-none" } >
                                <ul>
                                  <li><span><img src={img_minusCircle} alt="minus"/></span> <span>2 Adult</span> <span><img src={img_plusCircle} alt="plus"/></span></li>
                                  <li><span><img src={img_minusCircle} alt="minus"/></span> <span>2 Children <small>(2-12years old)</small></span> <span><img src={img_plusCircle} alt="plus"/></span></li>
                                  <li><span><img src={img_minusCircle} alt="minus"/></span> <span>2 Infants <small>(0-2years old)</small></span> <span><img src={img_plusCircle} alt="plus"/></span></li>
                                </ul>
                                <button type="button" className="doneBtn">Done</button>
                                </div>
                            </div>
                         </div>
                      </div>
        
                      <div className="d-flex flex-row smallColumn  flightSearch">
                        <div className="flex-column">
                            <div className="selectTakeOff">
                                <img src={img_takeOff} />
                                <input type="text" className="" placeholder="Departure From"/>
                            </div>
                         </div>
                         <div className="flex-column">
                           <div className="selectTakeOff">
                              <img src={img_Landing} />
                              <input type="text" className="" placeholder="Arriving At"/>
                           </div>
                         </div>
                         <div className="flex-column">
                           {this.state.wayOfTravel == 'round' ? <div className="seleTravelDate roundTripDate align-self-start">
                              <img src={img_Calendar}/> 
                              <input type="date"  name="carDropDate" min={new Date().toISOString().substr(0,10)} value={this.state.carDropDate} onChange={this.handleCarFormDataChange}  className="dateInput borderRig" placeholder="Departure Date"/>
                              <input type="date"  name="carDropDate" min={new Date().toISOString().substr(0,10)} value={this.state.carDropDate} onChange={this.handleCarFormDataChange}  className="dateInput" placeholder="Return Date"/>
                          </div>  : <div className="seleTravelDate align-self-start">
                              <img src={img_Calendar}/> 
                              <input type="date"  name="carDropDate" min={new Date().toISOString().substr(0,10)} value={this.state.carDropDate} onChange={this.handleCarFormDataChange}  className="dateInput" placeholder="Departure Date"/>
                          </div>

                           }
                           
                          
                         </div>
                      </div>
                     {this.state.wayOfTravel == "multi" && this.state.multiFlightAdd.length > 0 && this.state.multiFlightAdd.map((number,i) =>
                         <div className="d-flex flex-row smallColumn  flightSearch" key={i}>
                         <div className="flex-column">
                             <div className="selectTakeOff">
                                 <img src={img_takeOff} />
                                 <input type="text" className="" placeholder="Departure From"/>
                             </div>
                          </div>
                          <div className="flex-column">
                            <div className="selectTakeOff">
                               <img src={img_Landing} />
                               <input type="text" className="" placeholder="Arriving At"/>
                            </div>
                          </div>
                          <div className="flex-column">
                             <div className="seleTravelDate addFlightDates align-self-start">
                               <img src={img_Calendar}/> 
                               <input type="date"  name="carDropDate" min={new Date().toISOString().substr(0,10)} value={this.state.carDropDate} onChange={this.handleCarFormDataChange}  className="dateInput" placeholder="Departure Date"/>
                           </div>
                          <span className="closeFlight" id={i+1} onClick={this.removeFlights} > <i className="fas fa-times"></i></span>
                          </div>
                       </div>
                      )}
                    {this.state.wayOfTravel == "multi" ? <div className="addAnotherBtn"><button type="button" className=""  onClick={this.addAnotherFlight} disabled={addFlightField}><i className="fas fa-plus"></i> Add Another Flight </button></div> : '' }
                    <div class="text-right mt-3"><button type="button" className="searchBtn"  onClick={this.handleFlightSearch}>Search <img src={img_SearchIcon} alt="searchIcon"/></button></div>
                  </React.Fragment>
                )
      case this._tabs[1].to:
        return (
          <React.Fragment>
            <h3 className="tabHeadText">Search and Save on Hotels</h3>
            <div className="selectDivsAl">
              <div className="form-group mb-2">
                <div className="seleboxs">
                  <LocationSearch placeholder="Where..?" onSearch={this.getSearchInfo} />
                  <i className="fas fa-map-marker-alt locationIcon" onClick={() => this.getSearchInfo(staticBounds)} />
                </div>
              </div>
              <div className="form-group d-flex flex-row smallColumn mb-0">
                <div className="seleboxs1 flex-column  align-self-start mr-2">
                  <img src={img_Calendar} className="calendImg" alt="" />
                  <input
                    type="text"
                    className="dateInput borderRig"
                    placeholder="Sat,Oct 20"
                    onChange={this.handleStartDate}
                    value={startDate}
                    min={this.getToday()}
                    onClick={() => this.setState({ isCalendar: true })}
                  />
                  <input
                    type="text"
                    className="dateInput"
                    placeholder="Fri,Oct 26"
                    onChange={this.handleEndDate}
                    value={endDate}
                    min={startDate}
                    onClick={() => this.setState({ isCalendar: true })}
                  />
                  {isCalendar && <div onMouseLeave={() => {this.setState({isCalendar:false})}}><DateRangePicker
                    value={this.state.value}
                    onSelect={this.onSelect}
                    minimumDate={new Date()}
                    numberOfCalendars={2}
                    stateDefinitions={stateDefinitions}
                    dateStates={dateRanges}
                    defaultState="available"
                  />
                  </div>}
                </div>
                <div className="seleboxs2 d-flex flex-row justify-content-center mr-2">
                  <div className="form-group selectDivsAl">
                    <select
                      className="sele-width"
                      value={room}
                      onChange={(e) => this.setState({ room: e.target.value })}
                    >
                      {_map(roomCounts, (each, i) => (
                        <option key={i} value={i + 1}>{i + 1} Room{i =="0" ? '' : 's'}</option>
                      ))}
                    </select>
                  </div>
                </div>
                <div className="d-flex flex-row smallColumn">
                  <div className="form-group selectDivsAl mr-2">
                    <select
                      className="sele-width"
                      value={adult}
                      onChange={(e) => this.setState({ adult: e.target.value })}
                    >
                      <option value="1">1 Adult</option>
                      <option value="2">2 Adults</option>
                      <option value="3">3 Adults</option>
                      <option value="4">4 Adults</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <select
                      className="sele-width"
                      value={child}
                      onChange={(e) => this.setState({
                        child: e.target.value,
                         childAgeValues: this.state.childAgeValues.length > 4?this.state.childAgeValues.splice(4,this.state.childAgeValues.length) :this.state.childAgeValues.concat("")
                      })}
                    
                    >
                      <option value="0">
                        No Children
                       </option>
                      <option value="1">1 Child </option>
                      <option value="2">2 Children </option>
                      <option value="3">3 Children </option>
                      <option value="4">4 Children </option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group d-flex flex-row smallColumn justify-content-end">
                <div className="d-flex flex-row">
                  {this.state.childAgeValues && this.state.childAgeValues.length > 0 && this.state.child>0 && this.renderChildAge()}
                </div>
              </div>

            </div>
            <div className="text-right">
              <button
                type="button"
                className="searchBtn"
                onClick={this.handleSearch}
                disabled={ this.state.searchPayload == null }
              >
                Search <img src={img_SearchIcon} alt="searchIcon" />
              </button>
               
            </div>
            {/* disabled={!this.state.searchPayload} */}
          </React.Fragment>
        );
        case this._tabs[2].to:return(

          <React.Fragment  >
            <div >
              <h3 className="tabHeadText">Search and Save on Car Deals </h3> 
              <div className="d-flex flex-row smallColumn carSearchOption">
                <div  className="flex-column">
									
											<div   id='pickUpLocation' className="seleLocation">
												{/* <input type="text"  className="" placeholder="Pick-up Location"/> */}
                        <LocationSearch placeholder="Pickup Location" onSearch={this.getSearchInfoPickUp} />
												<i className="fas fa-map-marker-alt locationIcon"></i>
											</div>
							  	
							   </div>
                 <div className="flex-column">
                     <div  id='carPickUpDate' className="seleDate align-self-start">
                        <img src={img_Calendar}/> 

                     <input
                    type="text"
                    className="dateInput"
                    placeholder="Fri,Oct 26"
                    value={this.state.carPickUpDate}
                    onClick={() => this.setState({ isCarPickUpDateCalender: true, isCarDropDateCalender:false })}
                     />

                        {this.state.isCarPickUpDateCalender &&
                        <div onMouseLeave={() => {this.setState({isCarPickUpDateCalender:false})}}>
                        <DateRangePicker
                        selectionType="single"
                        value={this.state["pickUpDateValue"]}
                        onSelect={(pickUpDateValue)=>{
                          this.setState({
                            carDropDateValue:pickUpDateValue,
                            pickUpDateValue,
                            carPickUpDate: moment(pickUpDateValue._d).format("MM/DD/YYYY"),
                            //carDropDateValue:"",
                            carDropDate:"",
                            isCarPickUpDateCalender: false
                          })
                        }}
                        minimumDate={moment().add(1,'days')}
                       
                  /></div>
                        }
                      
                      </div>
                 </div>
                 <div className="flex-column">
                   <div  id='carPickUpTime' className="seleTime align-self-start">
                      <img src={img_clock} /> 

                           <select name="carPickUpTime" value={this.state.carPickUpTime} onChange={this.handleCarFormDataChange}>  
        <option value="00:00">{moment("00:00","hh:mm").format("LT")}</option>
        <option value="00:30">{moment("00:30","hh:mm").format("LT")}</option>
        <option value="01:00">{moment("01:00","hh:mm").format("LT")}</option>
        <option value="01:30">{moment("01:30","hh:mm").format("LT")}</option>
        <option value="02:00">{moment("02:00","hh:mm").format("LT")}</option>
        <option value="02:30">{moment("02:30","hh:mm").format("LT")}</option>
        <option value="03:00">{moment("03:00","hh:mm").format("LT")}</option>
        <option value="03:30">{moment("03:30","hh:mm").format("LT")}</option>
        <option value="04:00">{moment("04:00","hh:mm").format("LT")}</option>
        <option value="04:30">{moment("04:30","hh:mm").format("LT")}</option>
        <option value="05:00">{moment("05:00","hh:mm").format("LT")}</option>
        <option value="05:30">{moment("05:30","hh:mm").format("LT")}</option>
        <option value="06:00">{moment("06:00","hh:mm").format("LT")}</option>
        <option value="06:30">{moment("06:30","hh:mm").format("LT")}</option>
        <option value="07:00">{moment("07:00","hh:mm").format("LT")}</option>
        <option value="07:30">{moment("07:30","hh:mm").format("LT")}</option>
        <option value="08:00">{moment("08:00","hh:mm").format("LT")}</option>
        <option value="08:30">{moment("08:30","hh:mm").format("LT")}</option>
        <option value="09:00">{moment("09:00","hh:mm").format("LT")}</option>
        <option value="09:30">{moment("09:30","hh:mm").format("LT")}</option>
        <option value="10:00">{moment("10:00","hh:mm").format("LT")}</option>
        <option value="10:30">{moment("10:30","hh:mm").format("LT")}</option>
        <option value="11:00">{moment("11:00","hh:mm").format("LT")}</option>
        <option value="11:30">{moment("11:30","hh:mm").format("LT")}</option>
        <option value="12:00">{moment("12:00","hh:mm").format("LT")}</option>
        <option value="12:30">{moment("12:30","hh:mm").format("LT")}</option>
        <option value="13:00">{moment("13:00","hh:mm").format("LT")}</option>
        <option value="13:30">{moment("13:30","hh:mm").format("LT")}</option>
        <option value="14:00">{moment("14:00","hh:mm").format("LT")}</option>
        <option value="14:30">{moment("14:30","hh:mm").format("LT")}</option>
        <option value="15:00">{moment("15:00","hh:mm").format("LT")}</option>
        <option value="15:30">{moment("15:30","hh:mm").format("LT")}</option>
        <option value="16:00">{moment("16:00","hh:mm").format("LT")}</option>
        <option value="16:30">{moment("16:30","hh:mm").format("LT")}</option>
        <option value="17:00">{moment("17:00","hh:mm").format("LT")}</option>
        <option value="17:30">{moment("17:30","hh:mm").format("LT")}</option>
        <option value="18:00">{moment("18:00","hh:mm").format("LT")}</option>
        <option value="18:30">{moment("18:30","hh:mm").format("LT")}</option>
        <option value="19:00">{moment("19:00","hh:mm").format("LT")}</option>
        <option value="19:30">{moment("19:30","hh:mm").format("LT")}</option>
        <option value="20:00">{moment("20:00","hh:mm").format("LT")}</option>
        <option value="20:30">{moment("20:30","hh:mm").format("LT")}</option>
        <option value="21:00">{moment("21:00","hh:mm").format("LT")}</option>
        <option value="21:30">{moment("21:30","hh:mm").format("LT")}</option>
        <option value="22:00">{moment("22:00","hh:mm").format("LT")}</option>
        <option value="22:30">{moment("22:30","hh:mm").format("LT")}</option>
        <option value="23:00">{moment("23:00","hh:mm").format("LT")}</option>
        <option value="23:30">{moment("23:30","hh:mm").format("LT")}</option>
                           </select>
                       {/* <input type="time" name="carPickUpTime" value={this.state.carPickUpTime} onChange={this.handleCarFormDataChange} className="dateInput" placeholder="Pick-Up Time"/> */}
                    </div>
                 </div>
              </div>

              <div className="d-flex flex-row smallColumn  carSearchOption">
                <div className="flex-column">
								
											<div   id="dropOffLocation" className="seleLocation">
												{/* <input type="text" className="" placeholder="Drop-Off Location (same as Pick-Up)"/> */}
                        <LocationSearch placeholder="Dropoff Location "  onSearch={this.getSearchInfoDropOff} />
												<i className="fas fa-map-marker-alt locationIcon"></i>
											</div>
							  	
							   </div>
                 <div className="flex-column">
                     <div  id="carDropDate" className="seleDate align-self-start">
                        <img src={img_Calendar}/>

                        <input
                    type="text"
                    className="dateInput"
                    placeholder="Fri,Oct 26"
                    value={this.state.carDropDate}
                    onClick={() => this.setState({ isCarDropDateCalender: true,isCarPickUpDateCalender:false })}
                  />

                        {this.state.isCarDropDateCalender &&
                        <div onMouseLeave={() => {this.setState({isCarDropDateCalender:false})}}>
                        <DateRangePicker
                        selectionType="single"
                        value={this.state["carDropDateValue"]}
                        onSelect={(carDropDateValue)=>{
                          this.setState({
                            carDropDateValue,
                            carDropDate: moment(carDropDateValue._d).format("MM/DD/YYYY"),
                            isCarDropDateCalender: false
                          })
                        }}
                       minimumDate={moment(this.state.carPickUpDate).add(1,'days')}
                       maximumDate={moment(this.state.carPickUpDate).add(90,'days')}
    
                  />
                  </div>}
          
                      </div>
                 </div>
                 <div className="flex-column">
                   <div  id="carDropTime" className="seleTime align-self-start">
                      <img src={img_clock}/> 
                        {/* <input type="time"  name="carDropTime" value={this.state.carDropTime} onChange={this.handleCarFormDataChange}  className="dateInput" placeholder="Drop-Off Time"/>  */}
                     <select  name="carDropTime" value={this.state.carDropTime} onChange={this.handleCarFormDataChange} className="dateInput" >
                     <option value="00:00">{moment("00:00","hh:mm").format("LT")}</option>
                     <option value="00:30">{moment("00:30","hh:mm").format("LT")}</option>
                     <option value="01:00">{moment("01:00","hh:mm").format("LT")}</option>
                     <option value="01:30">{moment("01:30","hh:mm").format("LT")}</option>
                     <option value="02:00">{moment("02:00","hh:mm").format("LT")}</option>
                     <option value="02:30">{moment("02:30","hh:mm").format("LT")}</option>
                     <option value="03:00">{moment("03:00","hh:mm").format("LT")}</option>
                     <option value="03:30">{moment("03:30","hh:mm").format("LT")}</option>
                     <option value="04:00">{moment("04:00","hh:mm").format("LT")}</option>
                     <option value="04:30">{moment("04:30","hh:mm").format("LT")}</option>
                     <option value="05:00">{moment("05:00","hh:mm").format("LT")}</option>
                     <option value="05:30">{moment("05:30","hh:mm").format("LT")}</option>
                     <option value="06:00">{moment("06:00","hh:mm").format("LT")}</option>
                     <option value="06:30">{moment("06:30","hh:mm").format("LT")}</option>
                     <option value="07:00">{moment("07:00","hh:mm").format("LT")}</option>
                     <option value="07:30">{moment("07:30","hh:mm").format("LT")}</option>
                     <option value="08:00">{moment("08:00","hh:mm").format("LT")}</option>
                     <option value="08:30">{moment("08:30","hh:mm").format("LT")}</option>
                     <option value="09:00">{moment("09:00","hh:mm").format("LT")}</option>
                     <option value="09:30">{moment("09:30","hh:mm").format("LT")}</option>
                     <option value="10:00">{moment("10:00","hh:mm").format("LT")}</option>
                     <option value="10:30">{moment("10:30","hh:mm").format("LT")}</option>
                     <option value="11:00">{moment("11:00","hh:mm").format("LT")}</option>
                     <option value="11:30">{moment("11:30","hh:mm").format("LT")}</option>
                     <option value="12:00">{moment("12:00","hh:mm").format("LT")}</option>
                     <option value="12:30">{moment("12:30","hh:mm").format("LT")}</option>
                     <option value="13:00">{moment("13:00","hh:mm").format("LT")}</option>
                     <option value="13:30">{moment("13:30","hh:mm").format("LT")}</option>
                     <option value="14:00">{moment("14:00","hh:mm").format("LT")}</option>
                     <option value="14:30">{moment("14:30","hh:mm").format("LT")}</option>
                     <option value="15:00">{moment("15:00","hh:mm").format("LT")}</option>
                     <option value="15:30">{moment("15:30","hh:mm").format("LT")}</option>
                     <option value="16:00">{moment("16:00","hh:mm").format("LT")}</option>
                     <option value="16:30">{moment("16:30","hh:mm").format("LT")}</option>
                     <option value="17:00">{moment("17:00","hh:mm").format("LT")}</option>
                     <option value="17:30">{moment("17:30","hh:mm").format("LT")}</option>
                     <option value="18:00">{moment("18:00","hh:mm").format("LT")}</option>
                     <option value="18:30">{moment("18:30","hh:mm").format("LT")}</option>
                     <option value="19:00">{moment("19:00","hh:mm").format("LT")}</option>
                     <option value="19:30">{moment("19:30","hh:mm").format("LT")}</option>
                     <option value="20:00">{moment("20:00","hh:mm").format("LT")}</option>
                     <option value="20:30">{moment("20:30","hh:mm").format("LT")}</option>
                     <option value="21:00">{moment("21:00","hh:mm").format("LT")}</option>
                     <option value="21:30">{moment("21:30","hh:mm").format("LT")}</option>
                     <option value="22:00">{moment("22:00","hh:mm").format("LT")}</option>
                     <option value="22:30">{moment("22:30","hh:mm").format("LT")}</option>
                     <option value="23:00">{moment("23:00","hh:mm").format("LT")}</option>
                     <option value="23:30">{moment("23:30","hh:mm").format("LT")}</option>
                           </select>
                    </div>
                 </div>
              </div>
              <div className="pickuplocation">
                        <p className="">If different from pick up location</p>
                </div>
              <div className="d-flex flex-row smallColumn  carSearchOption">
                  <div className="flex-column">
                      <label style={{fontSize:' 16px',marginBottom:' 5px'}}>Driver Age</label>
                      <div  id="driverAge" className="seleAge">
                          <img src={img_age} alt="age icon"/>
                          {/* <input type="numbet" name="driverAge" value={this.state.driverAge} onChange={this.handleCarFormDataChange} ></input> */}
                          <select name="driverAge" value={this.state.driverAge} onChange={this.handleCarFormDataChange}>
                          {Array(100).fill(100).map((value,index)=>{
                            if(index>=18 && index <= 80){
                              return <option value={index}>{index}</option>
                            }
                          })}
                           </select>
                          {/*<option value="">Age</option></select> */}
                      </div>
                  </div>
                  <div className="flex-column">
                  <label style={{fontSize:' 16px',marginBottom:' 5px'}}>Nationality</label>
                  <div id="nationality" className="seleAge selectCountry">
                      <img src={img_country} alt="Country icon"/>
                    <Select className="countrySelectOption"
                      options={this.state.options}
                      value={this.state.nationality}
                      onChange={this.changeHandlerNationality}
                    />
                  </div> 
                    </div>
                </div>

              
                <span style={{color:"red"}}>  <strong>{this.state.errorCarValidation} </strong></span>
                
						<div class="text-right"><button type="button" onClick={this.carHandleSearch} className="searchBtn">Search <img src={img_SearchIcon} alt="searchIcon"/></button></div>
           </div>
          </React.Fragment>
        )
        
      default:
        return <React.Fragment>
          <div style={{ textAlign: "center" }}>
            <img src={img_progress} style={style} />
            <div className="noHotelText">
              {this._tabs.find(each => each.to === this.props.path)["lable"]} LIMITED ACCESS
              </div>
            <NavLink
              to={"/home"}
              href=""
              className="forgotLink commonLink"
            >
              Signup
            </NavLink>
          </div>
        </React.Fragment>;
    }
  };

  renderChildAge = () => {
    let { child, childAgeValues, childCounts } = this.state;
    let Age = childAgeValues && childAgeValues.length > 0 && Array(parseInt(child)).fill('');
    // let Age = childAgeValues && childAgeValues.length > 0 && childAgeValues;
       
    return (
      <React.Fragment>
        <div className="form-group">

         {child == 1 && <label className="age-box borLeft">Child Aged:</label>} 
         {child > 1 && <label className="age-box borLeft">Children Aged:</label>}
        </div>
        {childAgeValues && childAgeValues.length > 0 && Age.map((each, i) => {
          return (
            <div key={i} className="form-group">
              <select
                className="age-box"
                onChange={(e) => {
                  childAgeValues[i] = e.target.value;
                  this.forceUpdate()
                }}
              >
                {this.state.childAgeValues.length > 0 && <option value={this.state.childAgeValues[i] || 0}>{this.state.childAgeValues[i] || "Age"}</option>}
                {childCounts.map((each, i) => {
                  return (
                    <option key={i} value={i + 1} >{i + 1}  </option>
                  )
                })}
              </select>
            </div>
          )
        })}
      </React.Fragment>
    )
  }

}


const mapStateToProps = state => ({
  isSearching: state.hotelReducer.isSearching
});

const mapDispatchToProps = dispatch => ({
  searchHotel: searchInfo => dispatch(searchHotel(searchInfo))
});

export default
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withRouter(SearchTool));
