import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import moment from "moment";
import { profileImageChange, upload } from "../../../service/dashboard/action";
import DashboardRightPane from "./DashboardRightPane";
import img_manuser from "../../../asset/images/dashboard/man-user.png";
import img_history from "../../../asset/images/dashboard/history.png";
import img_settings from "../../../asset/images/dashboard/settings.png";
import img_xeniapp from "../../../asset/images/dashboard/xeniapp.png";
import img_payment from "../../../asset/images/dashboard/payment.png";
import img_heart from "../../../asset/images/dashboard/heart.png";
import img_sachine from "../../../asset/images/dashboard/sachine.jpg";
import img_xennies from "../../../asset/images/dashboard/logo-xennies.png";
import img_user from "../../../asset/images/dashboard/download.jpg";
import img_subscription from '../../../asset/images/dashboard/subscription.png';
import img_helpicon from '../../../asset/images/dashboard/helpicon.png';
import swal from "sweetalert";

class DashBoardContent extends Component {
  // fileUpload = e => {
  //   console.log('file', e.target.files[0])
  //   let profile_img = e.target.files[0];
  // }
  constructor() {
    super();
    this.state = {
      model: false,
      file: "",
      fileImage: "",
      error: "",
      errorValid:null,
    };
  }
  imageUpload = e => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    console.log("file",file.type)
    if(file.type=="image/png" || file.type=="image/jpg" || file.type == "image/jpeg" )
    {
    this.setState({ errorValid: false });
    if (file.size < 5000000)
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result,
          fileImage: URL.createObjectURL(file)
        });
      };
  }
  else{
    this.setState({ errorValid:true });
    swal("Only .JPG and .PNG  files are  allowed")
  }

    reader.readAsDataURL(file);

    // this.setState({
    //   file: readAsDataURL(event.target.files[0])
    // });
  };
  handleUpload = () => {
    this.setState({ model: !this.state.model });
  };
  
  uploadImageSubmit = (e) => {
    e.preventDefault();

    // const payload = {
    //   userPhoto: this.state.file,
    //   email: "prakash@yopmail.com",
    //   fileToDelete: "uploads/userPhoto-1546429500670"
    // };
    if(this.state.file)
    {
    
   if(this.state.errorValid==false)
  {
    if (this.state.file.size < 5000000) {
      const { email, profile_image, image_path } = this.props.loginDetails;
      // const { model } = this.props.uploadImage;

      var formData = new FormData();
      formData.append("userPhoto", this.state.file);
      formData.append("email", email);
      formData.append("fileToDelete", profile_image ? profile_image : null);
      this.props.upload(formData);
      this.setState({ model: false ,file:""});
    } }
    else {
      this.setState({
        error:  "Maximum file size is 5MB"
      });
    } 
    setTimeout(() => {
      this.setState({ error: "" });
    }, 1000);
  }
  };
  
  render() {
    const userSession = JSON.parse(sessionStorage.getItem('loginInfo'));
    
    const { pathname } = this.props.history.location;
    const { loginDetails } = this.props;
    const createdDate = moment(userSession.created_date).format("MMMM YYYY");
    const {imagePreviewUrl}=this.state
   
   // const {email,profile_image,image_path}=this.props.loginDetails
   const {email,profile_image,image_path} = userSession
   const image = this.props.uploadImage && this.props.uploadImage.image_path !== undefined && this.props.uploadImage.image_path;
    // const image = this.props.uploadImage && this.props.uploadImage.image_path !== undefined && this.props.uploadImage.image_path;
    // console.log("createdDate", this.props.loginDetails, this.props.uploadImage);
    // const { email, profile_image, image_path } = this.props.loginDetails;
    // const image_path_uploadImage = this.props.uploadImage.image_path;
    // var reader = new FileReader();
    // var url = this.state.file && reader.readAsDataURL(this.state.file);
    // console.log("url", profile_image, image_path, this.props.uploadImage);
     console.log("sdfsdffsf==",this.state)
    return (
      <section className="dashSection">
       
        <div className="container">
          <div className="dashContainer">
            <div className="dashTitleBg">
              <h3>{this.getTitle(pathname)}</h3>
            </div>
            <div className="dashLeftSide">
              <div className="profileView">
                <div className="profileImgBg">
                  {/* <img
                    src={
                      this.props.uploadImage.image_path ? image_path_uploadImage : !profile_image ?  image_path:img_user
                    }
                    alt=""
                  />{" "} */}
                  {<img src={image ? image : profile_image? image_path: img_user}/>}
                  <span className="uploadIcon" onClick={this.handleUpload}>
                    <i className="fas fa-upload" />
                  </span>
                </div>
  
                <h4>{userSession.name}</h4>
                <p>{userSession.email}</p>
                <p>Member since {createdDate}</p>
              </div>
              <ul className="dashboardMenuItems">
                <li
                  className={pathname === "/dashboard/overview" ? "active" : ""}
                  onClick={() => this.props.history.push("/dashboard/overview")}
                >
                  <span>
                    <img src={img_manuser} />
                  </span>{" "}
                  <h4>Overview</h4>
                </li>
                <li
                  className={pathname === "/dashboard/my-trips" ? "active" : ""}
                  onClick={() => this.props.history.push("/dashboard/my-trips")}
                >
                  <span>
                    <img src={img_heart} />
                  </span>{" "}
                  <h4>My Trips</h4>
                </li>
                <li
                  className={
                    pathname === "/dashboard/wish-list" ? "active" : ""
                  }
                  onClick={() =>
                    this.props.history.push("/dashboard/wish-list")
                  }
                >
                  <span>
                    <img src={img_history} />
                  </span>{" "}
                  <h4>Wishlist</h4>
                </li>
                <li
                  className={pathname === "/dashboard/payment" ? "active" : ""}
                  onClick={() => this.props.history.push("/dashboard/payment")}
                >
                  <span>
                    <img src={img_payment} />
                  </span>{" "}
                  <h4>Payment Method</h4>
                </li>
                <li
                  className={
                    pathname === "/dashboard/xeni-coin" ? "active" : ""
                  }
                  onClick={() =>
                    this.props.history.push("/dashboard/xeni-coin")
                  }
                >
                  <span>
                    <img src={img_xennies} />
                  </span>{" "}
                  <h4>Xennies</h4>
                
                </li>
                <li
                  className={pathname === "/dashboard/profile" ? "active" : ""}
                  onClick={() => this.props.history.push("/dashboard/profile")}
                >
                  <span>
                    <img src={img_settings} />
                  </span>{" "}
                  <h4>Profile Setting</h4>
                </li>
                <li
                  className={pathname === "/dashboard/mySubscription" ? "active" : ""}
                  onClick={() => this.props.history.push("/dashboard/mySubscription")}
                >
                  <span>
                    <img src={img_subscription} />
                  </span>{" "}
                  <h4>My Subscription</h4>
                </li>
                <li>
                <span>
                    <img src={img_helpicon} />
                  </span>{" "}
                      <a
                      target="_blank" 
                        href="http://help.xeniapp.com/support/home"><h4>Support</h4>
                   </a>
                
                </li>
              </ul>
            </div>
            <DashboardRightPane />
          </div>
        </div>
        {this.state.model === true && (
          <div
            className="modal backgroundDark"
            id="myAddCard"
            style={{ display: "block" }}
          >
            <div className="modal-dialog myNewAddCard">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Upload Profile</h4>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    onClick={this.handleUpload}
                  >
                    &times;
                  </button>
                </div>
                <div className="modal-body">
                  <div className="cardDetailForm">
                    <div className="uploadPreview">
                      {/* <img
                        src={
                          this.state.fileImage
                            ? this.state.fileImage
                            : !profile_image
                            ? img_user
                            : image_path_uploadImage
                        }
                        alt="preview image"
                      /> */}
                      <img src={imagePreviewUrl?imagePreviewUrl: img_user} alt="previe image" />
                    </div>
                    <div className="form-group">
                      <input
                         
                        type="file"
                        id="imageId"
                        className="chooseInput"
                        onChange={this.imageUpload}
                         accept=".png,.jpg,.jpeg"  
                      />
                      <div className="uploadButton">
                        <button
                          type="button"
                          className={this.state.errorValid == false ? "searchBtn" : "searchBtn cursorNotAllowed"}
                          onClick={this.uploadImageSubmit}
                          disabled={this.state.errorValid}
                        >
                          Upload
                        </button>
                      </div>
                      {this.state.error && <span style={{color:"red"}}>{this.state.error}</span>}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </section>
    );
  }

  getTitle = pathName => {
    switch (pathName) {
      case "/dashboard/overview":
        return "overview";
      case "/dashboard/payment":
        return "payment method";
      case "/dashboard/profile":
        return "profile settings";
      case "/dashboard/my-trips":
        return "my trips";
      case "/dashboard/xeni-coin":
        return "Xennies";
      case "/dashboard/wish-list":
        return "my wishlist";
        case "/dashboard/mySubscription":
        return "My Subscription";
      default:
        break;
    }
  };
}
const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  uploadImage: state.dashboardReducer.uploadImage
});

const mapDispatchToProps = dispatch => ({
  profileImageChange: profile_img => dispatch(profileImageChange),
  upload: data => dispatch(upload(data))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashBoardContent)
);


DashBoardContent.defaultProps ={
  loginDetails:{

  }
}
