import React, { Component } from "react";
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import DBWishListCarContent from "./DBWishListCarContent";
import DBWishListHotelContent from "./DBWishListHotelContent";
import DBWishListFlightContent from "./DBWishListFlightContent";
import img_Road from '../../../asset/images/Road-GraphicWish.png';
import openTab from "../../../asset/images/dashboard/resize.png";
import img_delete from '../../../asset/images/delete.png';
import img_edit from '../../../asset/images/pencil.png';
import img_Expanded from "../../../asset/images/Itinerary-Expanded-Lines.png";
import img_Time from '../../../asset/images/Time.svg';
import img_calender from '../../../asset/images/dashboard/calender.png';
import img_flight from '../../../asset/images/New folder/plane.png';
import img_car from '../../../asset/images/New folder/car.png';
import img_hotel from '../../../asset/images/New folder/hotel-building.png';
import img_activies from '../../../asset/images/New folder/activities.png';
import img_DateArrow from "../../../asset/images/Date Arrow.png";
import img_Arrival from '../../../asset/images/Arrival.svg';
import img_Departure from '../../../asset/images/Departure.svg';
import img_thumb from '../../../asset/images/dashboard/thumb-up-sign.png';
import img_tick from '../../../asset/images/dashboard/checked.png';
import { connect } from "react-redux";

import { viewWishList } from "../../../service/addCart/action";
import queryString from "query-string";
import moment from 'moment';
import URL from "../../../asset/configUrl"
import axios from "../../../service/Axios"

const TYPE_OF_VIEW = {
  FLIGHT: "FLIGHT",
  HOTEL: "HOTEL",
  CAR: "CAR",
  ACTIVITIES: "ACTIVITIES",
  PRIVATE: "PRIVATE"
};
class DBWishList extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: {} };
  }

  componentDidMount(){

    if(this.props.loginDetails){
       const { email } = this.props.loginDetails;
       this.props.getWishList(email)
    }
     
  }
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  state = {
    typeofView: TYPE_OF_VIEW["FLIGHT"]
  };
  handleCar = value => {
    this.setState({
      typeOfView: TYPE_OF_VIEW[value]
    });
  };

  
  renderCar =(carData)=>{
      return(
          <div>
    <div className="wishListCard">
        <div className="wishListCardTitle">
            <span> <img src={img_car}/></span>
            <h3>Cars</h3>
        </div>
        <div className="wishListCardBody">
            <h3><img src={img_car} alt="hotel"/>{carData.bookingData.vehicle.name}</h3>
            <ul className="commonPickDrop">
                <li> <img src={img_calender} /> </li>
                <li>
                <span className="bordered"><b>{moment(carData.searchString.carPickUpDate).format('DD MMM')}</b>  <small>{moment(carData.searchString.carPickUpDate).format('dddd')}</small></span>
                  <span className="commonTime"><img src={img_Time} /> {moment(carData.searchString.carPickUpTime,"hh:mm").format("LT")}</span> 
                </li>
                <li><img src={img_DateArrow} alt="arrow"/> </li>
                <li><span className="bordered"><b>{moment(carData.searchString.carDropDate).format('DD MMM')}</b>  <small>{moment(carData.searchString.carDropDate).format('dddd')}</small></span>
                    <span className="commonTime"> <img src={img_Time} />  {moment(carData.searchString.carDropTime,"hh:mm").format("LT")}</span> 
                </li>
            </ul>
            {/* <ul className="totalAmount">
               <li>
                
                <span><b className="d-block">ADD ON</b>GPS Navigational Device</span> <span> $104.00</span></li>
              <li><span>Passenger</span> <span>2</span></li>
              <li><span>Taxs & Fees</span> <span>$22.02</span></li> 
              <li><span>Total Cost</span> <span>$450.02</span></li>
            </ul> */}
        </div>
      </div>
      </div>
      )
  }

  renderHotel=(hotelData)=>{
      return(
          <div>
               <div className="wishListCard">
                            <div className="wishListCardTitle">
                                <span> <img src={img_hotel}/></span>
                                <h3>Hotel</h3>
                            </div>
                            <div className="wishListCardBody">
                                <h3><img src={img_hotel} alt="hotel"/> {hotelData.title}</h3>
                                <ul className="commonPickDrop">
                                    <li> <img src={img_calender} /> </li>
                                    <li>
                                     <span className="bordered"><b>{moment(hotelData.searchString.checkin).format('DD MMM')}</b>  <small>{moment(hotelData.searchString.checkin).format('dddd')}</small></span>
                                      {/* <span className="commonTime"><img src={img_Time} />  12.00pm</span>  */}
                                    </li>
                                    <li><img src={img_DateArrow} alt="arrow"/> </li>
                                    <li>
                                    <span className="bordered"><b>{moment(hotelData.searchString.checkout).format('DD MMM')}</b>  <small>{moment(hotelData.searchString.checkout).format('dddd')}</small></span>
                                     {/* <span className="commonTime"><img src={img_Time} />  12.00pm</span>  */}
                                   </li>
                                </ul>
                                {/* <ul className="totalAmount">
                                  <li>
                                    
                                    <span>GPS Navigational Device</span> <span> $104.00</span></li>
                                  <li><span>Passenger</span> <span>2</span></li>
                                  <li><span>Taxs & Fees</span> <span>$22.02</span></li>
                                  <li><span>Total Cost</span> <span>$450.02</span></li>
                                </ul> */}
                            </div>
                        </div>
                        {/* <div className="wishListNameYour">
                            <h5>Name Your Price</h5>
                            <div className="priceRate"> $ <input type="text" /></div>
                        </div> */}
                   
              </div>
      )
  }

  renderFlight=()=>{
      return(
          <div>
                       <div className="wishListCard">
                            <div className="wishListCardTitle">
                                <span> <img src={img_flight}/></span>
                                <h3>Flight</h3>
                            </div>
                            <div className="wishListCardBody">
                                <ul className="FlightStatus borderbtm">
                                      <li>
                                        <span className="flightDet"><img src={img_Departure} /> Frt,Oct 17</span>
                                        <span className="flightInfo"><img src={img_Time} /> <span className="bordered">5.39pm  <small>Steward Intl(SWF)</small></span></span>
                                      </li>
                                      <li><img src={img_DateArrow} alt="arrow"/> </li>
                                      <li>
                                         <span className="flightDet"><small>(1 Stop) </small> 5h 52m</span>
                                          <span className="flightInfo"> <span className="bordered">10.39pm  <small>George Bush Intl(IAH)</small></span></span>
                                      </li>
                                </ul>
                                <ul className="FlightStatus">
                                      <li>
                                        <span className="flightDet"><img src={img_Arrival} /> Frt,Oct 17</span>
                                        <span className="flightInfo"><img src={img_Time} /> <span className="bordered">5.39pm  <small>Steward Intl(SWF)</small></span></span>
                                      </li>
                                      <li><img src={img_DateArrow} alt="arrow"/> </li>
                                      <li>
                                         <span className="flightDet"><small>(1 Stop) </small> 5h 52m</span>
                                          <span className="flightInfo"><span className="bordered">10.39pm  <small>George Bush Intl(IAH)</small></span></span>
                                      </li>
                                </ul>
                            </div>
                        </div>
                        {/* <div className="wishListNameYour">
                            <h5>Name Your Price</h5>
                            <div className="priceRate"> $ <input type="text" /></div>
                        </div> */}
              </div>
      )
  }

  renderActivities=()=>{
      return(
          <div>
        <div className="wishListCard">
        <div className="wishListCardTitle">
            <span> <img src={img_activies}/></span>
            <h3>Activities</h3>
        </div>
        <div className="wishListCardBody">
            <h3><img src={img_car} alt="hotel"/>Hudson Highland Trail</h3>
            <ul className="commonPickDrop">
                <li> <img src={img_calender} /> </li>
                <li>
                 <span className="bordered"><b>OCT 20</b>  <small>SATURDAY</small></span>
                  
                </li>
                <li><img src={img_Time} className="time" alt="time"/> </li>
                <li><span className="bordered">11.30am - 02.00pm</span>
                   
                </li>
            </ul>
            <p><b>Guest :</b> 2</p>
            <ul className="totalAmount">
             
              <li><span>Passenger</span> <span>2</span></li>
              <li><span>Taxs & Fees</span> <span>$22.02</span></li>
              <li><span>Total Cost</span> <span>$450.02</span></li>
            </ul>
        </div>
    </div>
    </div>
      )
  }

  onDeleteWishListData=async (wishListData)=>{
    await axios.post(URL.DELETE_WISHLIST,{idValue:wishListData._id})
    if(this.props.loginDetails){
        const { email } = this.props.loginDetails;
        this.props.getWishList(email)
     }
  }

  onBookingHandle(bookingData){
   if(bookingData.type=="hotel"){
     this.props.history.push( "/hotel/rooms?" + queryString.stringify({...bookingData.searchString,wishList:true}));
   }else if(bookingData.type=="car"){
    this.props.history.push('/car/search?'+queryString.stringify(bookingData.searchString))
   }

  }
  render() {
    const { typeOfView } = this.state;
    const { wishList }=this.props;
    return (
   
      // New Design changes
      <div className="dashRightSide align-self-start">
         <div className="d-flex flex-wrap resWrap" >
           
              { wishList && wishList.length ==0 && <h6>

                 No items added to the wishlist. 
              </h6>}
                {wishList && wishList.map((wishListData,index)=>{
                    
                    let viewToDisplay;
                    if(wishListData.type=="hotel"){
                         viewToDisplay=this.renderHotel(wishListData)
                    }else if(wishListData.type=="car"){
                         viewToDisplay=this.renderCar(wishListData)
                    }
                   if( this.state.collapse["wishlist"+index] == null ||  this.state.collapse["wishlist"+index] == undefined){
                    let collapse=this.state.collapse
                    collapse["wishlist"+index]=false
                    this.setState({
                      collapse
                    })
                   }
                  
                    return(
                        <div className="flex-column wishListCollapse">
                        <div className="wishListCollapseTitle" color="primary" >
                        <div className="wishListTitle">
                          <h3 title={wishListData.title}>{wishListData.title}</h3>
                          {/* <img src={img_edit}/> */}
                        </div> 
                        <img src={img_Road} className="roadImgSty"/>
                              {/* <span className="openNewTab"><img src={openTab}/></span> */}
                              <span onClick={()=>this.onDeleteWishListData(wishListData)} className="delete"><img src={img_delete}/></span>
                              <span className="expanded" onClick={()=>{
                                  let collapse=this.state.collapse
                                  collapse["wishlist"+index]=!collapse["wishlist"+index]
                                  this.setState({
                                    collapse
                                  })
                              }} > <img src={img_Expanded} alt="" /></span>
                        </div>
                        <Collapse isOpen={this.state.collapse["wishlist"+index]}>
                          <Card>
                            <CardBody className="wishListCollapsecontent">
                                {viewToDisplay}
                                <div className="wishListTotalAmount">
                                    {/* <h2>TOTAL : $ {wishListData.price}</h2> */}
                                </div>
                                <div className="wishListBtnGroup">
                                    {/* <button type="button" className="searchBtn">Save <img src={img_thumb} alt="thumb"/></button> */}
                                    <button type="button" className="searchBtn book" onClick={this.onBookingHandle.bind(this,wishListData)}>Book <img src={img_tick} alt="tick"/></button>
                                </div>
                            </CardBody>
                          </Card>
                        </Collapse>   
                        </div>
                    )
                })
                
              }
          </div>
        </div>
      
    );
  }
}

const mapStateToProps = state => ({
    wishList: state.addcartReducer.wishListArray,
    wishlistStatus: state.addcartReducer.wishlistStatus,
    loginDetails: state.loginReducer.loginDetails
  });
  
  const mapDispatchToProps = dispatch => ({
    getWishList: (data) => dispatch(viewWishList(data)),
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(DBWishList);
  
//export default DBWishList;
