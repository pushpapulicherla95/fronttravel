import React, { Component } from "react";
import { connect } from "react-redux";
import {withRouter, NavLink } from "react-router-dom";
import { reduxForm, Field } from "redux-form";
import InputField from "../../Fields/TextField";
import { newpasswordInfo } from  '../../../service/login/action'
import queryString from 'query-string';
import Cryptr from 'cryptr';

class NewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      model: true,
      email:""
    };
  }

  componentWillReceiveProps=(newProps)=>{
  console.log("newPropsnewPropsnewProps", newProps);
  }
   componentWillMount=()=>{
    const { emailId } = queryString.parse(this.props.location.search);
    console.log("email encrpted ",new Cryptr('myTotalySecretKey').decrypt(emailId))
      this.setState({email: new Cryptr('myTotalySecretKey').decrypt(emailId)})
   }

  modelClose = () => {
    this.setState({ model: false });
  };
  newpasswordSubmit=(value)=>{
    const payload = {
      emailId: this.state.email,
      password: value.newPassword,
      confirm_password: value.confirmPassword
    };
    this.props.newpasswordInfo(payload);
    this.modelClose();
    this.props.history.push("/home")
  }

  render() {
    const { handleSubmit } = this.props;
     
    return (
      <div
        className="modal backgroundDark"
        id="myModal1"
        style={{ display: "block" }}
      >
        <div className="modal-dialog signInPopup">
          {this.state.model && (
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">FORGOT PASSWORD</h4>
                <button
                  type="button"
                  /* onClick={this.modelClose} */
                  className="close"
                >
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={handleSubmit(this.newpasswordSubmit)}>
                  <div className="signInForm">
                    <div className="form-group mailIcon">
                      <Field
                        name="newPassword"
                        type="password"
                        value=""
                        label="New Password"
                        component={InputField}
                        placeholder="New Password"
                        className="form-control"
                      />
                    </div>
                    <div className="form-group mailIcon">
                      <Field
                        name="confirmPassword"
                        type="password"
                        value=""
                        label="Confirm Password"
                        component={InputField}
                        placeholder="Confirm Password"
                        className="form-control"
                      />
                    </div>

                    <div className="form-group">
                      <button className="searchBtn" type="submit">
                        SEND
                      </button>
                    </div>
                  </div>
                     <NavLink
                        to={"/home"}
                        href=""
                        className="forgotLink commonLink"
                      >
                       GO BACK HOMEPAGE
                      </NavLink>
                  {/* <a href="/">GO BACK HOMEPAGE</a> */}
                </form>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const forgetValidate = formProps => {
  const errors = {};

 if (!formProps.newPassword) {
    errors.newPassword = "Required";
  } 
  if(!formProps.confirmPassword)
  {
    errors.confirmPassword="Required"
  }else if(!(formProps.newPassword === formProps.confirmPassword))
  {
     errors.confirmPassword="Password mismatch" 
  }
  return errors;
};

const mapStateToProps = state => ({
  loginDetails: state.loginReducer.loginDetails,
  newPasswordDetails: state.loginReducer.newPasswordDetails
});

const mapDispatchToProps = dispatch => ({
  newpasswordInfo: value => dispatch(newpasswordInfo(value))
});
// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(
//   reduxForm({
//     form: "NewPassword",
//     validate: forgetValidate
//   })(NewPassword)
// );

export default withRouter(connect(mapStateToProps ,mapDispatchToProps)(
     reduxForm({
       form: "NewPassword",
      validate: forgetValidate
     })(NewPassword)
   ));