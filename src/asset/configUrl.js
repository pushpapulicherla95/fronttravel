
const API = "http://localhost:8080/api"; //-local
// const API = "http://192.168.2.44:8080/api"; //-local
//const  API = "https://xeniapp.com:8443/api"
const configUrl = {
  init: `${API}/init`,
  hotel: {
    HOTEL_SEARCH: `${API}/hotel/search`,
    HOTEL_FILTER: `${API}/hotel/filter`,
    ROOM_SEARCH: `${API}/hotel/getRoom`,
    ROOM_PRICE: `${API}/hotel/getPrice`,
    ROOM_BOOKING: `${API}/hotel/Booking`,
    ROOM_CANCEL: `${API}/hotel/status/cancellation`,
    ROOM_SEARCHSTATELESS: `${API}/hotel/getroominit`
  },
  // HOTEL_INIT: `${API}/hotel`,
  // HOTEL_STATUS: `${API}/status`,
  // HOTEL_RESULTS: `${API}/search`

  //CAR
  CAR_SEARCH: `${API}/car/search`,
  CAR_FILTER: `${API}/car/filter`,
  CAR_PRICE: `${API}/car/getPrice`,
  CAR_BOOKING: `${API}/car/booking`,
  CAR_BOOKING_CANCEL: `${API}/car/cancellation`,
  CAR_MYTRIPS: `${API}/car/mytrips`,

  //user
  USER_GOOGLE_LOGIN: `${API}/user/googleLogin`,
  USER_SIGNUP: `${API}/user/signUp`,
  USER_LOGIN: `${API}/user/login`,
  USER_FORGETPASSWORD: `${API}/user/forgotPasswordLink?emailId=`,
  USER_NEWPASSWORD: `${API}/user/forgotPassword`,

  card: {
    CARD_GET: `${API}/user/getSavedCardList?email=`,
    CARD_ADD: `${API}/user/createCard`,
    CARD_DELETE: `${API}/user/deleteSavedCard`
  },
  //dashboard
  PROFILE_EDIT: `${API}/user/editProfile`,
  GET_PROFILE: `${API}/user/getProfile?email=`,
  CHANGE_PASSWORD: `${API}/user/changePassword`,
  TRIP_DETAILS: `${API}/user/getUserTransactions`,
  RECENT_ACTIVITY: `${API}/user/getRecentActivity?email=`,
  GET_REFUND_AMOUNT: `${API}/hotel/getRefundAmount `,
  CANCELLED_BOOKING: `${API}/hotel/status/cancel `,
  GOPROREGISTER: `${API}/subscription/addSubscription`,
  GOPROPLANLIST:`${API}/subscription/planList`,
  UPLOAD_PICTURE:`${API}/user/uploadPicture`,
// https://xeniapp.com:8443/api/user/uploadPicture
  //Itinery
  ADD_WISHLIST: `${API}/wishlist/save`,
  VIEW_WISHLIST: `${API}/wishlist/all?email=`,

  //User Subscription
  GET_SUBSCRIPTIONLIST :`${API}/subscription/getUserSubscription?email=`,
  DELETE_SUBSCRIPTION:`${API}/subscription/deleteSubscription`,
  UPDATE_CARD:`${API}/subscription/updateCard`,
  
  DELETE_WISHLIST: `${API}/wishlist/delete`,

  //contactForm
  CONTACT_DETAILS:`${API}/user/contactDetails`

};
export default configUrl;