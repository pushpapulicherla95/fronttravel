import React,{Component} from 'react';
import $ from 'jquery';

class ScrollToTop extends Component{
    constructor(){
        super()
        
            
    }
   scrollTop = () =>{
      $("html, body").animate({ scrollTop: 0 }, 1000);
   }

   componentWillMount() {
       console.log("scrolltoTOp");
       var visible = false;
       //Check to see if the window is top if not then display button
       $(window).scroll(function() {
         var scrollTop = $(this).scrollTop();
         if (!visible && scrollTop > 200) {
           $(".scrollTopBtn").fadeIn();
           visible = true;
         } else if (visible && scrollTop <= 200) {
           $(".scrollTopBtn").fadeOut();
           visible = false;
         }
       });
   }

    render(){
        console.log("scrollBorRender")
        return(
            <div className="scrollTopBtn" title="Scroll to top" onClick={this.scrollTop}><i class="fas fa-arrow-up"></i></div>
        )
    }

}export default ScrollToTop