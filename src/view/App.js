import React, { Component } from 'react';
// import { Switch, Route, Redirect } from 'react-router-dom';
import { NavLink, Switch, Route } from 'react-router-dom';

import '../asset/scss/style.scss'
import Home from './Home';
import MainPage from './MainPage';
import Dashboard from './dashboard';
import AboutUs from '../component/AboutUs';
import Landing from './Landing';
import AuthRoute from '../component/Auth';
import cancelBooking from '../component/container/mainPage/CancelBooking';
import CancelledBooking from  '../component/container/mainPage/CancelledBooking';

import BookingConfirmation from '../component/container/mainPage/BookingConfirmation';

import CarBookingConfirmation from '../component/container/mainPage/CarBookingConfirmation';
import CarCancelBooking from '../component/container/mainPage/CarCancelBooking';
import CarCancelledBooking from  '../component/container/mainPage/CarCancelledBooking';
import NewPassword from '../component/container/login/NewPassword'

import ContactUs from '../component/ContactUs';
import Loading from "../component/Loading";
import PaymentPage from "../component/PaymentPage";
import TermsAndConditions from '../component/TermsAndCondition';
import PrivacyAndPolicy from '../component/PrivacyAndPolicy';

import MultipleBookingView from '../component/presentational/MultipleBooking/MultipleBookingView'

import StatelessHotelConfirmation from '../component/container/mainPage/MultipleBooking/StatelessHotelConfirmation'
import ErrorPage from './ErrorPage';
//import StatelessHotelConfirmation from '../component/container/mainPage/MultipleBooking/StateLessCarConfirmation'
export default class App extends Component {
  constructor(props){
    super(props)
  }

  render() {
   
    return (
      <div>
      {<Loading/>}
      <Switch>
        <AuthRoute path="/dashboard" component={Dashboard} />
        {/* <Redirect exact path="/" to="/home" /> */}
        <Route exact path='/' component={Landing}></Route>

        <Route path={"/home"} component={Landing} />
        <Route path={"/hotel"} {...this.props} component={MainPage} />
        <Route path={"/aboutUs"} component={AboutUs} />
        <Route path={"/flight"} component={MainPage} />
        <Route path={"/car"} {...this.props} component={MainPage} />
        <Route path={"/package"} component={MainPage} />
        <Route path={"/activity"} component={MainPage} />
        <Route path={"/newPassword"} component={NewPassword} />
        <AuthRoute path={"/cancelBooking"} component={cancelBooking} />
        <AuthRoute path={"/CancelledBooking"}  component={CancelledBooking} />
        <AuthRoute  path={"/bookingconfirmation"} component={BookingConfirmation} />
        <AuthRoute path={"/carbookingconfirmation"}  component={CarBookingConfirmation}  />
        <AuthRoute path={"/carCancelBooking"} component={CarCancelBooking} />
        <AuthRoute  path={"/carCancelledBooking"}  component={CarCancelledBooking}  />
        <Route path={"/contactUS"} component={ContactUs} />
        <Route path={"/payment"} component={PaymentPage} />
        <Route path={"/multiplebooking"} component={MultipleBookingView} />
        <Route path={"/multipleconfirmation"} component={StatelessHotelConfirmation} />
        <Route path={"/termsandconditions"} component={TermsAndConditions} />
        <Route path={"/privacypolicy"} component={PrivacyAndPolicy} />
        <Route component={ErrorPage} />
        {/* <Route path="/home" exact component={Home}/> */}
      </Switch>
      </div>
    );
  }
}

